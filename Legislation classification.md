# The legislations can be categorized as follows:

1) Labour laws enacted by the Central Government, where the Central Government has the sole responsibility for enforcement.
2) Labour laws enacted by Central Government and enforced both by Central and State Governments.
3) Labour laws enacted by Central Government and enforced by the State Governments.
4) Labour laws enacted and enforced by the various State Governments which apply to respective States.

## (a) Labour laws enacted by the Central Government, where the Central Government has the sole responsibility for enforcement

1. The Employees’ State Insurance Act, 1948
2. The Employees’ Provident Fund and Miscellaneous Provisions Act,1952
3. The Dock Workers (Safety, Health and Welfare) Act, 1986
4. The Mines Act, 1952
5. The Iron Ore Mines, Manganese Ore Mines and Chrome Ore Mines Labour Welfare (Cess)Act, 1976
6. The Iron Ore Mines, Manganese Ore Mines and Chrome Ore Mines Labor Welfare Fund Act, 1976
7. The Mica Mines Labour Welfare Fund Act, 1946
8. The Beedi Workers Welfare Cess Act, 1976
9. The Limestone and Dolomite Mines Labour Welfare Fund Act, 1972
10. The Cine Workers Welfare (Cess) Act, 1981
11. The Beedi Workers Welfare Fund Act, 1976
12. The Cine Workers Welfare Fund Act, 1981

## (b) Labour laws enacted by Central Government and enforced both by Central and State Governments

13. The Child Labour (Prohibition and Regulation) Act, 1986.
14. The Building and Other Constructions Workers’ (Regulation of Employment and Conditions of Service) Act, 1996.
15. The Contract Labour (Regulation and Abolition) Act, 1970.
16. The Equal Remuneration Act, 1976.
17. The Industrial Disputes Act, 1947.
18. The Industrial Employment (Standing Orders) Act, 1946.
19. The Inter-State Migrant Workmen (Regulation of Employment and Conditions of Service) Act, 1979.
20. The Labour Laws (Exemption from Furnishing Returns and Maintaining Registers by Certain Establishments) Act, 1988
21. The Maternity Benefit Act, 1961
22. The Minimum Wages Act, 1948
23. The Payment of Bonus Act, 1965
24. The Payment of Gratuity Act, 1972
25. The Payment of Wages Act, 1936
26. The Cine Workers and Cinema Theatre Workers (Regulation of Employment) Act, 1981
27. The Building and Other Construction Workers Cess Act, 1996
28. The Apprentices Act, 1961
29. Unorganized Workers Social Security Act, 2008
30. Working Journalists (Fixation of Rates of Wages Act, 1958
31. Merchant Shipping Act, 1958
32. Sales Promotion Employees Act, 1976
33. Dangerous Machines (Regulation) Act, 1983
34. Dock Workers (Regulation of Employment) Act, 1948
35. Dock Workers (Regulation of Employment) (Inapplicability to Major Ports) Act, 1997
36. Private Security Agencies (Regulation) Act, 2005

## (c) Labour laws enacted by Central Government and enforced by the State Governments

37. The Employers’ Liability Act, 1938
38. The Factories Act, 1948
39. The Motor Transport Workers Act, 1961
40. The Personal Injuries (Compensation Insurance) Act, 1963
41. The Personal Injuries (Emergency Provisions) Act, 1962
42. The Plantation Labour Act, 1951
43. The Sales Promotion Employees (Conditions of Service) Act, 1976
44. The Trade Unions Act, 1926
45. The Weekly Holidays Act, 1942
46. The Working Journalists and Other Newspapers Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955
47. The Workmen’s Compensation Act, 1923
48. The Employment Exchange (Compulsory Notification of Vacancies) Act, 1959
49. The Children (Pledging of Labour) Act 1938
50. The Bonded Labour System (Abolition) Act, 1976
51. The Beedi and Cigar Workers (Conditions of Employment) Act, 1966
