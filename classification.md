Classification of LABOUR LAWS in India
Labour Laws may be classified under the following heads:

## I. Laws related to Industrial Relations such as:
1. Trade Unions Act, 1926
2. Industrial Employment Standing Order Act, 1946.
3. Industrial Disputes Act, 1947.

## II. Laws related to Wages such as:
4. Payment of Wages Act, 1936
5. Minimum Wages Act, 1948
6. Payment of Bonus Act, 1965.
7. Working Journalists (Fixation of Rates of Wages Act, 1958

## III. Laws related to Working Hours, Conditions of Service and Employment such as:
8. Factories Act, 1948.
9. Plantation Labour Act, 1951.
10. Mines Act, 1952.
11. Working Journalists and other Newspaper Employees’ (Conditions of Service and Misc. Provisions) Act, 1955.
12. Merchant Shipping Act, 1958.
13. Motor Transport Workers Act, 1961.
14. Beedi & Cigar Workers (Conditions of Employment) Act, 1966.
15. Contract Labour (Regulation & Abolition) Act, 1970.
16. Sales Promotion Employees Act, 1976.
17. Inter-State Migrant Workmen (Regulation of Employment and Conditions of Service) Act, 1979.
18. Dock Workers (Safety, Health & Welfare) Act, 1986.
19. Building & Other Construction Workers (Regulation of Employment & Conditions of Service) Act, 1996.
20. Building and Other Construction Workers Welfare Cess Act, 1996
21. Cine-Workers and Cinema Theatre Workers (Regulation of Employment) Act, 1981
22. Dangerous Machines (Regulation) Act, 1983
23. Dock Workers (Regulation of Employment) Act, 1948
24. Dock Workers (Regulation of Employment) (Inapplicability to Major Ports) Act, 1997
25. Employment of Manual Scavengers and Construction of Dry Latrines (Prohibition) Act, 1993
26. Industrial Employment (Standing Orders) Act, 1946
27. Mines and Mineral (Development and Regulation Act, 1957
28. Plantation Labour Act, 1951
29. Private Security Agencies (Regulation) Act, 2005

## IV. Laws related to Equality and Empowerment of Women such as:
30. Maternity Benefit Act, 1961
31. Equal Remuneration Act, 1976.

## V. Laws related to Deprived and Disadvantaged Sections of the Society such as:
32. Bonded Labour System (Abolition) Act, 1976
33. Child Labour (Prohibition & Regulation) Act, 1986
34. Children (Pledging of Labour) Act, 1933

## VI. Laws related to Social Security such as:
35. Workmen’s Compensation Act, 1923.
36. Employees’ State Insurance Act, 1948.
37. Employees’ Provident Fund & Miscellaneous Provisions Act, 1952.
38. Payment of Gratuity Act, 1972.
39. Employers’ Liability Act, 1938
40. Beedi Workers Welfare Cess Act, 1976
41. Beedi Workers Welfare Fund Act, 1976
42. Cine workers Welfare Cess Act, 1981
43. Cine Workers Welfare Fund Act, 1981
44. Fatal Accidents Act, 1855
45. Iron Ore Mines, Manganese Ore Mines and Chrome Ore Mines Labour Welfare Cess Act, 1976
46. Iron Ore Mines, Manganese Ore Mines and Chrome Ore Mines Labour Welfare Fund Act, 1976
47. Limestone and Dolomite Mines Labour Welfare Fund Act, 1972
48. Mica Mines Labour Welfare Fund Act, 1946
49. Personal Injuries (Compensation Insurance) Act, 1963
50. Personal Injuries (Emergency Provisions) Act, 1962
