# Central Labour Laws and their classification

A list of the important Central labour enactments can be grouped on the basis of their thrust to specific areas under the following heads:-

## I. Laws related to Industrial Relations such as:
- (1) Trade Unions Act, 1926. (C)
- (2) Industrial Employment Standing Order Act, 1946. (B)
- (3) Industrial Disputes Act, 1947. (B)

## II. Laws related to Wages such as:
- (1) Payment of Wages Act, 1936. (B)
- (2) Minimum Wages Act, 1948. (B)
- (3) Payment of Bonus Act, 1965. (B)

## III. Laws related to working hours, conditions of services and Employment such as:
- (1) Factories Act, 1948. (C)
- (2) Plantation Labour Act, 1951. (C)
- (3) Mines Act, 1952. (A)
- (4) Working Journalists and other Newspaper Employees’ (Conditions of Service and Misc. Provisions) Act, 1955. (C)
- (5) Motor Transport Workers Act, 1961. (C)
- (6) Beedi & Cigar Workers (Conditions of Employment) Act, 1966. (C)
- (7) Contract Labour (Regulation & Abolition) Act, 1970. (B)
- (8) Sales Promotion Employees Act, 1976. (C)
- (9) Inter-State Migrant Workmen (Regulation of Employment and conditions of Service) Act, 1979. (B)
- (10) The Employers Liability Act, 1938. (C)
- (11) Dock Workers (Safety, Health & Welfare) Act, 1986. (A)
- (12) Building & Other Construction Workers (Regulation of Employment & Conditions of Service) Act, 1996. (B)
- (13) Building & Other Construction Workers Cess Act, 1996.(B)
- (14) Cine Workers and Cinema Theatre Workers (Regulation of Employment) Act, 1981. (B)

## IV. Laws related to Equality and Empowerment of Women such as:
- (1) Maternity Benefit Act, 1961. (B)
- (2) Equal Remuneration Act, 1976. (B)

## V. Laws related to Deprived and Disadvantaged Sections of the Society such as:
- (1) Bonded Labour System (Abolition) Act, 1976. (C)
- (2) Child Labour (Prohibition & Regulation) Act, 1986. (B)
- (3) Children (Pledging of Labour) Act, 1933. (C)

## VI. Laws related to Social Security such as:
- (1) Workmen’s Compensation Act, 1923. (C)
- (2) Employees’ State Insurance Act, 1948. (A)
- (3) Employees’ Provident Fund & Miscellaneous Provisions Act, 1952. (A)
- (4) Payment of Gratuity Act, 1972. (B)

## VII. Laws related to Labour Welfare
- (1) Limestone & Dolomite Mines Labour Welfare Fund Act, 1972. (A)
- (2) Beedi Workers Welfare Fund Act, 1976. (A)
- (3) Beedi Workers Welfare Cess Act, 1976. (A)
- (4) Iron Ore Mines, Manganese Ore Mines & Chrome Ore Mines Labour Welfare Fund Act, 1976. (A)
- (5) Iron Ore Mines, Manganese Ore Mines & Chrome Ore Mines Labour Welfare Cess Act, 1976. (A)
- (6) Cine Workers Welfare Fund Act, 1981. (A)
- (7) Cine Workers Welfare Cess Act, 1981. (A)
- (8) The Mica Mines Labour Welfare Fund Act, 1946. (A)

## VIII. Laws related to Employment & Training
- (1) Employment Exchanges (Compulsory Notification of Vacancies) Act, 1959. (C)
- (2) Apprentices Act, 1961. (B)

## IX. Others
- (1) Weekly Holiday Act, 1942. (C)
- (2) Personal Injuries (Emergency) Provisions Act, 1962. (C)
- (3) Personal Injuries (Compensation Insurance) Act, 1963. (C)
- (4) Labour Laws (Exemption from Furnishing Returns and Maintaining Register by Certain Establishments) Act, 1988. (B)

**Note:-**

A = Labour Laws enacted and enforced by Central Government
B = Labour Laws enacted by Central and enforced by both
Central and State Governments
C = Labour Laws enacted by Central and enforced by State
Govt.
