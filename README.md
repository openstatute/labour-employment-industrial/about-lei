# About LEI

About Labour Employment and Industrial related laws


# Summary

All labour laws grouped into these categories based on its nature and purpose of enactment.

- Regulatory
- Industrial Relations
- Industrial Safety & Health
- Child & Women Labour
- Social Security
- Wages
- Labour Welfare
- Employment
- Labour/ Workmen Specific

### Regulatory
### Industrial Relations

---
<dl>  
  <dt>The Industrial Disputes Act, 1947</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Industrial Disputes (Central) Rules,1957</dt>
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---  
<dl>  
  <dt>The Industrial Employment (Standing Orders) Act, 1946</dt>
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Industrial Employment (Standing Orders) Rules, 1946</dt>
  <dd></dd><!-- For ammendment Capturing -->   
</dl>

---
<dl>  
  <dt>The Trade Unions Act, 1926</dt><!-- Title of the Act -->
  <dd>The Trade Unions (Amendments) Act, 2001</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Act -->
  <dd></dd><!-- For ammendment Capturing -->   
</dl> 

---
<dl>  
  <dt>The Plantation Labour Act, 1951</dt><!-- Title of the Act -->
  <dd></dd><!-- For ammendment Capturing -->   
</dl>

### Industrial Safety & Health

---
<dl>  
  <dt>The Factories Act, 1948</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Mines Act, 1952</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---  
<dl>  
  <dt>The Dock Workers (Safety, Health & Welfare) Act, 1986</dt>
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing -->   
</dl>

---
<dl>  
  <dt>The Mines Act, 1952</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

### Child & Women Labour

---
<dl>  
  <dt>Equal Remuneration Act, 1976</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>Equal Remuneration Rules, 1976</dt><!-- Title of the Rule -->  
  <dd>The Central Advisory Committee on Equal Remuneration Rules, 1991</dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Child and Adolescent Labour (Prohibition & Regulation) Act, 1986</dt><!-- Title of the Act -->  
  <dd>The Schedule (Amendment) to the Child and Adolescent Labour (Prohibition and Regulation) Act, 1986</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---  
<dl>  
  <dt>The Child and Adolescent Labour (Prohibition & Regulation) Act, 1986</dt>
  <dd>The Child Labour (Prohibition & Regulation) (Amendment) Act, 2016</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd>the Child Labour (Prohibition and Regulation) Amendment Rules, 2017</dd><!-- For ammendment Capturing -->   
</dl>

---
<dl>  
  <dt>Standard Operating Procedure (SOP) for Enforcement of the Child and Adolescent Labour (Prohibition and Regulation) Act,1986</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>Child Labour Act, 1986</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>Standard Operating Procedure (SOP) for Enforcement of the Child and Adolescent Labour (Prohibition and Regulation) Act,1986</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

### Social Security

---
<dl>  
  <dt>The Payment of Gratuity Act, 1972</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Payment of Gratuity Rules 1972</dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Maternity Benefit Act, 1961</dt><!-- Title of the Act -->  
  <dd>Maternity Benefit(Amendment) Act,2017</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>Employees Compensation Act,0000</dt><!-- Title of the Act -->  
  <dd>Employees Compensation(Amendment) Act,2017</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Personal Injuries (Emergency) Provisions Act, 1962</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Personal Injuries (Compensation Insurance) Act, 1963</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Employees Provident Fund & Miscellaneous Provisions Act, 1952</dt><!-- Title of the Act -->  
  <dd>The Employees’ Provident Fund & Miscellaneous Provisions (Amendment) Act, 1996</dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Employees State Insurance Act, 1948</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Employees Compensation Act, 1923</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>
---
---

### Wages

#### Minimum Wages
<dl>  
  <dt>The Minimum Wages Act, 1948</dt>  
  <dd></dd>
</dl>  
<dl>  
  <dt>The Minimum Wages (Central) Rules, 1950</dt>  
</dl> 

#### Payment of Wages
<dl>  
  <dt>The Payment of Wages Act, 1936</dt>  
  <dd>The Payment of Wages (AMENDMENT) Act, 2005</dd>
  <dd>The Payment of Wages (Amendment) Act, 2017</dd>
</dl>  
<dl>  
  <dt>	The Payment of Wages Rules, 1937</dt>  
</dl>  

#### Bonus
<dl>  
  <dt>The Payment of Bonus Act, 1965</dt>  
  <dd>The Payment of Bonus (Amendment) Act, 2007</dd>
  <dd>The Payment of Bonus (Amendment) Act, 2015</dd>
  <dd>The Payment of Bonus (Amendment) Ordinance, 2007</dd>
</dl>  
<dl>  
  <dt>The Payment of Bonus Rules, 1975</dt>  
  <dd>The Payment of Bonus (Amendment) Rules, 2014</dd>
  <dd>The Payment of Bonus (Amendment) Rules, 2016</dd>
</dl>  

---
---

### Labour Welfare

---
<dl>  
  <dt>Unorganised Workers Social Security Act 2008</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>Unorganised Workers Social Security Rules 2009</dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>


---
<dl>  
  <dt>The Bonded Labour System (Abolition) Act, 1976</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>


---
<dl>  
  <dt>The Inter-State Migrant Workmen (Regulation of Employment and Conditions of Service) Act, 1979</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>


---
<dl>  
  <dt>The Contract Labour (Regulation & Abolition) Act, 1970</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>


---
<dl>  
  <dt>The Beedi & Cigar Workers (Conditions of Employment) Act, 1966</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

--- 
---

### Employment and Training
#### Employment

---
<dl>  
  <dt>The Employment Exchanges (Compulsory Notification of Vacancies) Act, 1959</dt><!-- Title of the Act -->  
  <dd>The Employment Exchanges (Compulsory Notification of Vacancies) (Amendment) Act, 1960</dd><!-- For ammendment Capturing -->
  <dt>The Employment Exchanges (Compulsory Notification of Vacancies) Rule, 1960</dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

#### Training

##### The Aparantices 
<dl>  
  <dt></dt>  
  <dd></dd>
</dl>

---
---

### Labour/ Workmen Specific

##### Cine Workers 

---
<dl>  
  <dt>[The Cine Workers’ Welfare Fund Act, 1981](https://gitlab.com/openstatute/)</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Cine Workers Welfare Cess Act, 1981</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Cine Workers and Cinema Theatre Workers (Regulation of Employment) Act, 1981</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Cine Workers and Cinema Theatre Workers (Regulation of Employment) Rules, 1984</dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

##### Working Journalists and Other Newspaper Employees
---
<dl>  
  <dt>The Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Rules, 1957</dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing --> 
</dl>

---
<dl>  
  <dt>The Working Journalist (Fixation of Rates of Wages) Act, 1958</dt><!-- Title of the Act -->  
  <dd></dd><!-- For ammendment Capturing -->
  <dt>The Working Journalists (Fixation of Rates of Wages) Rules, 1958</dt><!-- Title of the Rule -->  
  <dd>The Working Journalists (Fixation of Rates of Wages)(Amendment) Rules, 1958</dd><!-- For ammendment Capturing --> 
  <dt></dt><!-- Title of the Rule -->  
  <dd></dd><!-- For ammendment Capturing -->
</dl>

---
---

