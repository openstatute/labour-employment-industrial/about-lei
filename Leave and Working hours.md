 <table cellpadding="0" border="1" style="width:1026px" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 1022px; height: 20px;" colspan="7">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:19px;">
			<p><strong>1</strong></p>
			</td><td rowspan="8" style="width: 151px; height: 19px;">
			<p>Andhra Pradesh</p>
			</td><td style="width: 386px; height: 19px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 19px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 39px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 39px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 39px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 58px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 58px;">
			<p>12 (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 58px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 58px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 58px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval for rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>60</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Overtime Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spread-over hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of normal wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 38px;" colspan="5">
			<p><strong><u>Ordinary rate of wages :</u></strong><br />
			Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:19px;">
			<p>2</p>
			</td><td rowspan="9" style="width: 151px; height: 19px;">
			<p>Assam</p>
			</td><td style="width: 386px; height: 19px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 19px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 38px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 38px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 19px;">
			<p>12 (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 19px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval for rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>16 (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Overtime Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Two hours in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>10.5 hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of normal wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 37px;" colspan="5">
			<p><strong>Year :</strong><br />
			Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 58px;" colspan="5">
			<p><strong>Ordinary rate of wages :</strong><br />
			Ordinary rate of wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the supply of meals and the concessional sale to employees of food grains and other articles as the employee is for the time being entitled to, but does not include bonus.</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:19px;">
			<p>3</p>
			</td><td rowspan="9" style="width: 151px; height: 19px;">
			<p>Bihar</p>
			</td><td style="width: 386px; height: 19px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 19px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 38px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 38px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 (half wages on production of medical certificate every year)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 (full pay every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval for rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 19px;">
			<p>18 (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 19px;">
			<p>45</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Maximum Overtime Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>One hrs in a day and fifty four hours in a week and the aggregate hours of overtime work shall not exceed one hundred and fifty hours in a year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 19px;" colspan="5" nowrap>
			<p><strong>Year :</strong><br />
			Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 57px;" colspan="5">
			<p><strong>Ordinary rate of wages :</strong><br />
			Ordinary rate of wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the supply of meals and the concessional sale to employees of food grains and other articles as the employee is for the time being entitled to, but does not include bonus.</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:57px;">
			<p><strong>4</strong></p>
			</td><td rowspan="8" style="width: 151px; height: 57px;">
			<p><strong>Chandigarh</strong></p>
			</td><td style="width: 386px; height: 57px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 57px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 57px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 57px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 19px;">
			<p>7</p>
			</td><td style="width: 66px; height: 19px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7</p>
			</td><td style="width: 66px; height: 38px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval for rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Overtime Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Ten hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the rate of normal wages calculated by the hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 883px; height: 57px;" colspan="5">
			<p><strong><u>Normal wages :</u></strong><br />
			Normal Wages means basic wages plus such allowances including the cash equivalent of the advantages accruing through the Concessional sale to workers of foodgrains and other articles as the worker is for the time being entitled to, but does not include bonus.</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p><strong>5</strong></p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p><strong>Chhattisgarh</strong></p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 38px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 38px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>No Provision for sick leave</p>
			</td><td style="width: 66px; height: 57px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Forty eight hours in a week and nine hours in a day in the shop; forty eight hours in a week and ten hours in a day in the commercial establishment</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 19px;">
			<p>14</p>
			</td><td style="width: 66px; height: 19px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Interval for rest</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Not Applicable</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>30 days (After 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not exceeding three months or 90 days</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Overtime Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 47px;">
			<p>&nbsp;</p>
			</td><td style="width: 789px; height: 47px;" colspan="4">
			<p><strong>Year :</strong><br clear="all" />
			Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p><strong>6</strong></p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p><strong>Dadra and Nagar Haveli</strong></p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 47px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 47px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 47px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 39px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 39px;">
			<p>9 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 39px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 39px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 39px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>6 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Earned / Priviledge Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p><br />
			15 days (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>45</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 39px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 39px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:39px;">
			<p><strong>7</strong></p>
			</td><td rowspan="7" style="width: 151px; height: 39px;">
			<p><strong>Daman &amp; Diu</strong></p>
			</td><td style="width: 386px; height: 39px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 39px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 39px;">
			<p><strong>Type of Leave</strong></p>
			</td><td style="width: 223px; height: 39px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 39px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>9 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>6 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p><strong>8</strong></p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p><strong>Daman &amp; Diu</strong></p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>9 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>6 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td rowspan="11" style="width:42px;height:20px;">
			<p><strong>9</strong></p>
			</td><td rowspan="11" style="width: 151px; height: 20px;">
			<p><strong>Delhi</strong></p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (sick leave or casual leave both combined)</p>
			</td><td rowspan="2" style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 38px;">
			<p>1 day (after one month of continuous employment)</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of half and hour</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (sick leave or casual leave both combined)</p>
			</td><td rowspan="2" style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Fifty four hours in a week and one hundred and fifty hours in year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 38px;">
			<p>1 day (after one month of continuous employment)</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>10.5 hours in any commercial establishment and twelve hours in any shop</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 12 months of continuous service)</p>
			</td><td rowspan="2" style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the rate of his normal remuneration calculated by the hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 38px;">
			<p>5 days (after 4 months of continuous service)</p>
			</td><td style="width: 157px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 38px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year :</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4" nowrap>
			<p>Year means calendar year.</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Normal wages :</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4" nowrap>
			<p>For the purpose of calculating the normal hourly wage the day shall be reckoned as consisting of eight hours</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;" nowrap>
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 19px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:19px;">
			<p>10</p>
			</td><td rowspan="8" style="width: 151px; height: 19px;">
			<p>Goa</p>
			</td><td style="width: 386px; height: 19px;" colspan="3">
			<p><strong>Leaves for Shops &amp; Establishment Act</strong></p>
			</td><td rowspan="2" style="width: 495px; height: 19px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>9 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>6 days (during 12 month of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 42px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 42px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p>11</p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p>Gujarat</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 76px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 76px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 76px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 76px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 76px;">
			<p>After five hours of work an interval of rest of at least half and hour, if he/she is employed in a commercial establishment engaged in any manufacturing process, and one hour in any other case</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Three hours in any week</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>5 days (For every 60 days worked)</p>
			</td><td rowspan="2" style="width: 66px; height: 38px;">
			<p>63</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>ELeve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 38px;">
			<p>21 days (For every 240 days worked)</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One and a half time the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year :</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4" nowrap>
			<p><br />
			Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:21px;">
			<p>12</p>
			</td><td rowspan="9" style="width: 151px; height: 21px;">
			<p>Haryana</p>
			</td><td style="width: 386px; height: 21px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 21px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (after 20 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the rate of normal wages calculated by the hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Year commencing on the first day of April</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Quarter</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Period of three months commencing on the first day of January, first day of April , first day of July and first day of October every year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Normal Wages</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Normal Wages means basic wages plus such allowances including the cash equivalent of the advantages accruing through the concessional sale to workers of food grains and other articles as the worker is for the time being entitled to , but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:20px;">
			<p>13</p>
			</td><td rowspan="9" style="width: 151px; height: 20px;">
			<p>Himachal Pradesh</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work, interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (after 20 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Ten hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the rate of normal wages calculated by an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year reckoned according to the British Calendar</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 37px;">
			<p><strong><u>Quarter</u></strong></p>
			</td><td style="width: 789px; height: 37px;" colspan="4">
			<p>Quarter means a period of three months commencing on the first day of January, first day of April, first day of July or first day of October every year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Normal Wages</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Normal wages means basic wages plus such allowances including the cash equivalent of the advantages accruing through the confessional sale to workers of food grains and other articles as any worker is for the time being entitled to but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p>14</p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p>JAMMU &amp; KASHMIR</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for sick leave</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work, interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>14 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Three hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>30 days (After every 12 months continuous employment)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not exceeding three months or 90 days</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year reckoned according to the British Calendar</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>15</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Jharkhand</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (half pay every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work, interval of rest of at least half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (full pay for every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty four hours in a week and One hundred and fifty hours in year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (after 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of April</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Ordinary rate of wages :</p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages, in relation to an employee means the basic rates of wages and such allowances as the employee is for the time being entitled to but does not include a bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>16</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Karnataka</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 (During the first twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After five hours of work, interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>NA</p>
			</td><td style="width: 66px; height: 38px;">
			<p>NA</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18&nbsp;&nbsp;&nbsp;&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the rate of normal wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Year</p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Normal Wages</p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Normal wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the concessional sale to workers of food grains and other articles, as the worker is for the time being entitled to, but does not include a bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>17</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Kerala</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Fifty hours for a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>24</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Ten and half hours in any day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 58px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 58px;" colspan="4">
			<p>Ordinary rate of wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the supply of meals and the concessional sale to employees of food grains and other articles, as the employee is for the time being entitled to, but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p>18</p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p>Madhya Pradesh</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 57px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 57px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Forty eight hours in a week and nine hours in a day in the shop; forty eight hours in a week and ten hours in a day in the commercial establishment</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for sick leave&nbsp;&nbsp;&nbsp;&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Not Applicable</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>14 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in any week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>30 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not exceeding three months or 90 days</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td rowspan="6" style="width:42px;height:20px;">
			<p>19</p>
			</td><td rowspan="6" style="width: 151px; height: 20px;">
			<p>Maharashtra</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for sick leave</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>8 Days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One hundred and twenty five hours in a period of three months</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 34px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 34px;">
			<p>5 Days (for every 60 days worked)</p>
			</td><td rowspan="2" style="width: 66px; height: 34px;">
			<p>45 Days</p>
			</td><td style="width: 157px; height: 34px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 34px;">
			<p>Ten and half hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 34px;">
			<p>18 Days (for every 240 days worked)</p>
			</td><td style="width: 157px; height: 34px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 34px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>20</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Manipur</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 157px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 20px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 495px; height: 40px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (For half wage after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Commercial Establishment - Seven hours in a day Shops - Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (Full pay for every year)&nbsp;&nbsp;&nbsp;&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After three hours of work, interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>30 days (After 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Not Applicable</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Period of twelve months of the Gregorian Calendar commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td rowspan="6" style="width:42px;height:20px;">
			<p>21</p>
			</td><td rowspan="6" style="width: 151px; height: 20px;">
			<p>Meghalaya</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during every twelve months of continuous service)&nbsp;&nbsp;&nbsp;&nbsp;</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work interval of rest of at least half one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during every twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Two hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>16 days (after 12 months of continuous service&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>No Applicable</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:20px;">
			<p>22</p>
			</td><td rowspan="9" style="width: 151px; height: 20px;">
			<p>Nagaland</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 157px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 20px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 495px; height: 40px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work, interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>16 days (after every 12 months continuous employment)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>30</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Two hours in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Ten and half hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year :</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p>23</p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p>Odisha</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for casual leave</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One hour in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (After 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rates of wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the concessional sale to workers of food-grains and other articles as the worker is for the time being entitled to but does not include a bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="7" style="width:42px;height:20px;">
			<p>24</p>
			</td><td rowspan="7" style="width: 151px; height: 20px;">
			<p>Puducherry</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work, interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Two hours in a day and Fifty four hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (after every 12 months continuous employment)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>24</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed.</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:20px;">
			<p>25</p>
			</td><td rowspan="9" style="width: 151px; height: 20px;">
			<p>Punjab</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half an hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>7 days</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (after 20 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the rate of normal wages calculated by the hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Year commencing on the first day of April</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Quarter</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Period of three months commencing on the first day of January, first day of April , first day of July and first day of October every year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong><u>Normal Wages</u></strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Normal Wages means basic wages plus such allowances including the cash equivalent of the advantages accruing through the concessional sale to workers of food grains and other articles as the worker is for the time being entitled to , but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>26</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Rajasthan</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for sick leave&nbsp; &nbsp;</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work interval of rest of at least half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>No provision for casual leave</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One hour in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>18 days (after 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>30</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>One and a half time the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the concessional sale to workers of foodgrains and other articles, as the worker is for the time being entitled to, but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>27</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Sikkim</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Nine hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After five hours of work, interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during twelve months of continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Three hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>20 days (after every 12 months continuous employment)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>60</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>ELeve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Year</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Year commencing on the first day of January</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>28</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Tamilnadu</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 157px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 20px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 495px; height: 40px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 57px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 57px;">
			<p>After four hours of work interval for rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45 Days</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 20px;" nowrap>
			<p>Rate of OT Wages&nbsp;</p>
			</td><td style="width: 337px; height: 20px;">
			<p>Twice the ordinary rate of normal wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>29</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Telangana</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 76px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 76px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 76px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 76px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 76px;">
			<p>After five hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 76px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 76px;">
			<p>12 days (during his first twelve months and further continuous service)</p>
			</td><td style="width: 66px; height: 76px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 76px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 76px;">
			<p>Six hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 240 days of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>60</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate of normal wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 20px;" nowrap>
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong><u>Ordinary rate of wages :</u></strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="8" style="width:42px;height:20px;">
			<p>30</p>
			</td><td rowspan="8" style="width: 151px; height: 20px;">
			<p>Tripura</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>14 days (half pay every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>112</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After six hours of work, interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>12 days (full pay for every year)</p>
			</td><td rowspan="2" style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One and half hours in a day and one hundred and twenty hours in a year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 57px;">
			<p>1 day (after one month of continuous employment)</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Ten and half hours in a day</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 12 months of continuous service)</p>
			</td><td rowspan="2" style="width: 66px; height: 38px;">
			<p>56</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twice the ordinary rate of wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 223px; height: 38px;">
			<p>5 days (after 4 months of continuous service)</p>
			</td><td style="width: 157px; height: 38px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 38px;" nowrap>
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;" nowrap>
			<p><strong>Ordinary rate of wages :</strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Ordinary rate of wages shall mean such rate of wages as may be calculated in the manner prescribed</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:20px;">
			<p>31</p>
			</td><td rowspan="9" style="width: 151px; height: 20px;">
			<p>Uttar Pradesh</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 6 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work, interval of rest of half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>10 days (subject to such conditions as may be prescribed)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Two hours in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Definitions:</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 19px;" nowrap>
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong>Quarter :</strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Quarter means a period of three consecutive months beginning on the 1st of January, the 1st of April, the 1st of July or the 1st of October</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p><strong>Ordinary rate of wages :</strong></p>
			</td><td style="width: 789px; height: 57px;" colspan="4">
			<p>Ordinary rate means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the concessional sale to employees of foodgrains and other articles, as the employee is for the time being entitled to, but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="9" style="width:42px;height:20px;">
			<p>32</p>
			</td><td rowspan="9" style="width: 151px; height: 20px;">
			<p>Uttarakhand</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 6 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five hours of work, interval of rest of half and hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 57px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 57px;">
			<p>10 days (subject to such conditions as may be prescribed)</p>
			</td><td style="width: 66px; height: 57px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 57px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 57px;">
			<p>Two hours in a day and fifty hours in a quarter</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>15 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>45</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Twelve hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 19px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p>Definitions:</p>
			</td><td style="width: 223px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 19px;">
			<p>&nbsp;</p>
			</td><td style="width: 337px; height: 19px;" nowrap>
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 19px;">
			<p><strong>Quarter :</strong></p>
			</td><td style="width: 789px; height: 19px;" colspan="4">
			<p>Quarter means a period of three consecutive months beginning on the 1st of January, the 1st of April, the 1st of July or the 1st of October</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p><strong>Ordinary rate of wages :</strong></p>
			</td><td style="width: 789px; height: 38px;" colspan="4">
			<p>Ordinary rate means the basic wages plus such allowances, including the cash equivalent of the advantage accruing through the concessional sale to employees of foodgrains and other articles, as the employee is for the time being entitled to, but does not include bonus</p>
			</td>
		</tr>
		<tr>
			<td rowspan="6" style="width:42px;height:20px;">
			<p>33</p>
			</td><td rowspan="6" style="width: 151px; height: 20px;">
			<p>West Bengal</p>
			</td><td style="width: 386px; height: 20px;" colspan="3">
			<p><strong>Leave for Shops &amp; Establishment</strong></p>
			</td><td style="width: 495px; height: 20px;" colspan="2">
			<p><strong>Working Hours for Shops &amp; Establishment</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 40px;">
			<p><strong>Type Of Leave</strong></p>
			</td><td style="width: 223px; height: 40px;">
			<p><strong>Leave Entitlement</strong></p>
			</td><td style="width: 66px; height: 40px;">
			<p><strong>Max Carry Forward Days</strong></p>
			</td><td style="width: 157px; height: 40px;">
			<p>Normal Working Hours</p>
			</td><td style="width: 337px; height: 40px;">
			<p>Eight and half hours in a day and forty eight hours in a week</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Sick Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>14 days (half pay every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>56</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Interval For Rest</p>
			</td><td style="width: 337px; height: 38px;">
			<p>After five and a half hours of work interval of rest of at least one hour</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Casual Leave (Days)</p>
			</td><td style="width: 223px; height: 38px;">
			<p>10 days (full pay for every year)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>Not Applicable</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Maximum Over Time Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>One and half hours in a day and one hundred twenty hours in a year</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 38px;">
			<p>Earned / Privileged Leave</p>
			</td><td style="width: 223px; height: 38px;">
			<p>14 days (after 12 months of continuous service)</p>
			</td><td style="width: 66px; height: 38px;">
			<p>28</p>
			</td><td style="width: 157px; height: 38px;">
			<p>Spreadover Hours</p>
			</td><td style="width: 337px; height: 38px;">
			<p>Ten and half hours in a day</p>
			</td>
		</tr>
		<tr>
			<td style="width: 91px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 223px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 66px; height: 20px;">
			<p>&nbsp;</p>
			</td><td style="width: 157px; height: 20px;">
			<p>Rate of OT Wages</p>
			</td><td style="width: 337px; height: 20px;">
			<p>Twice the ordinary rate wages</p>
			</td>
		</tr>
		<tr>
			<td style="width:42px;height:21px;">&nbsp;</td><td style="width: 151px; height: 21px;">&nbsp;</td><td style="width: 91px; height: 21px;">&nbsp;</td><td style="width: 223px; height: 21px;">&nbsp;</td><td style="width: 66px; height: 21px;">&nbsp;</td><td style="width: 157px; height: 21px;">&nbsp;</td><td style="width: 337px; height: 21px;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 1022px; height: 122px;" colspan="7">
			<p><strong>Disclaimer: The information contained in this website is for general information purposes only. The information is provided by www.mnc-kl.in, a property of MNC Management Solutions &amp; Kapson Law While we endeavour to keep the information up to date, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.<br />
			For any suggestions or feedback write to us at <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="25565055554a575165484b46084e490b4c4b">[email&#160;protected]</a></strong></p>
			</td>
		</tr>
	</tbody>
</table>
