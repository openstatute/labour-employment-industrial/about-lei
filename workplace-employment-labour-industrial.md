**DISCLAIMER: OpenStatute Initiative is providing you with this portal documents and contents. This document's information came from sources that OpenStatute Initiative considers to be trustworthy. Since these contents have not been independently verified, OpenStatute Initiative offers no claims or guarantees regarding their reliability, thoroughness, or correctness. The Principal Act and/or Amendment Act might not be accessible in some circumstances. Subsequent changes to the principal acts may or may not be present. For authoritative text, please get in touch with the pertinent state agency, or check out the most recent government report or gazette notification. For authoritative text, please get in touch with the respective authority, or check out the most recent government publications or the gazette notifications. Before acting on any of the data contained in this OpenStatute Initiative portal, anyone using it should seek their own professional and legal guidance. Any liability resulting from the use of this portal contant is not accepted by OpenStatute Initiative or any individuals associated with it. Any loss, damage, or distress caused to a person as a result of a decision made or not made based on this portal shall not in any way be the responsibility of OpenStatute Initiative or any individuals associated with it.**




# Summary

All labour laws grouped into these categories based on its nature and purpose of enactment.

- Regulatory
- Industrial Relations
- Industrial Safety & Health
- Child & Women Labour
- Social Security
- Wages
- Labour Welfare
- Employment
- Labour Reforms

### Regulatory
### Industrial Relations
### Industrial Safety & Health
### Child & Women Labour
### Social Security
---
---
### Wages

#### Minimum Wages
<dl>  
  <dt>The Minimum Wages Act, 1948</dt>  
  <dd></dd>
</dl>  
<dl>  
  <dt>The Minimum Wages (Central) Rules, 1950</dt>  
</dl> 

#### Payment of Wages
<dl>  
  <dt>The Payment of Wages Act, 1936</dt>  
  <dd>The Payment of Wages (AMENDMENT) Act, 2005</dd>
  <dd>The Payment of Wages (Amendment) Act, 2017</dd>
</dl>  
<dl>  
  <dt>	The Payment of Wages Rules, 1937</dt>  
</dl>  

#### Bonus
<dl>  
  <dt>The Payment of Bonus Act, 1965</dt>  
  <dd>The Payment of Bonus (Amendment) Act, 2007</dd>
  <dd>The Payment of Bonus (Amendment) Act, 2015</dd>
  <dd>The Payment of Bonus (Amendment) Ordinance, 2007</dd>
</dl>  
<dl>  
  <dt>The Payment of Bonus Rules, 1975</dt>  
  <dd>The Payment of Bonus (Amendment) Rules, 2014</dd>
  <dd>The Payment of Bonus (Amendment) Rules, 2016</dd>
</dl>  

---
---

### Labour Welfare
<dl>  
  <dt>The Unorganised Workers Social Security Act 2008</dt>  
  <dd></dd>
</dl>

--- 
---

### Employment and Training
#### Employment
<dl>  
  <dt>The Employment Exchanges (Compulsory Notification of Vacancies) Act, 1959</dt>  
  <dd>The Employment Exchanges (Compulsory Notification of Vacancies) (Amendment) Act, 1960</dd>
  <dt>The Employment Exchanges (Compulsory Notification of Vacancies) Rule, 1960</dt>  
  <dd></dd>
</dl>  

#### Training

##### The Aparantices 
<dl>  
  <dt></dt>  
  <dd></dd>
</dl>

---
---

### Labour/ Workmen Specific

##### Cine Workers 
<dl>
  <dt>[The Cine Workers’ Welfare Fund Act, 1981](https://gitlab.com/openstatute/about/-/edit/master/SUMMARY.md)</dt>  
  <dd></dd>
  <dt>	The Cine Workers Welfare Cess Act, 1981</dt>  
  <dd></dd>  
  <dt>The Cine Workers and Cinema Theatre Workers (Regulation of Employment) Act, 1981</dt>  
  <dd></dd>
  <dt>The Cine Workers and Cinema Theatre Workers (Regulation of Employment) Rules, 1984</dt>  
  <dd></dd>
</dl>  

---
---

* [Introduction](README.md)

# List of Enactments in the Ministry
## Central Labour Acts
1. The Employees’ Compensation Act, 1923
    1. The Employees’ Compensation (Amendment) Act 2017
1. The Trade Unions Act, 1926
1. **The Payment of Wages Act, 1936**
    1. The Payment of Wages (Amendment) Act 2017
1. **The Industrial Employment (Standing Orders) Act, 1946.**
    1. Notification dated 07.10.2016 (Amendment to Schedule)
1. The Industrial Disputes Act, 1947
1. The Minimum Wages Act, 1948
1. The Employees’ State Insurance Act, 1948
1. The Factories Act, 1948
1. The Plantation Labour Act, 1951
1. The Mines Act, 1952



