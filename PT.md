
<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid"><span style="background-color:#00FF00">Professional Tax Applicable States/UTs</span></span></h4>

<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 5.4pt 0in 5.4pt; mso-yfti-tbllook:1184; width:706.85pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Andhra Pradesh</span></p>
			</td><td valign="bottom" style="width:161.0pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Karnataka</span></p>
			</td><td valign="bottom" style="width:146.3pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Meghalaya</span></p>
			</td><td valign="bottom" style="width:195.45pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Punjab</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Assam</span></p>
			</td><td valign="bottom" style="width:161.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Kerala</span></p>
			</td><td valign="bottom" style="width:146.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Mizoram</span></p>
			</td><td valign="bottom" style="width:195.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Sikkim</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Bihar</span></p>
			</td><td valign="bottom" style="width:161.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Madhya Pradesh</span></p>
			</td><td valign="bottom" style="width:146.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Nagaland</span></p>
			</td><td valign="bottom" style="width:195.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Tamil Nadu</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Gujarat</span></p>
			</td><td valign="bottom" style="width:161.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Maharashtra</span></p>
			</td><td valign="bottom" style="width:146.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Odisha</span></p>
			</td><td valign="bottom" style="width:195.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Telangana</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Jharkhand</span></p>
			</td><td valign="bottom" style="width:161.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Manipur</span></p>
			</td><td valign="bottom" style="width:146.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Puducherry</span></p>
			</td><td valign="bottom" style="width:195.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Tripura</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5;mso-yfti-lastrow:yes;height:16.6pt">
			<td valign="bottom" style="width:204.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:16.6pt" width="272" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">West Bengal</span></p>
			</td><td valign="bottom" style="width:161.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="215" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">&nbsp;</span></p>
			</td><td valign="bottom" style="width:146.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="195" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">&nbsp;</span></p>
			</td><td valign="bottom" style="width:195.45pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.6pt" width="261" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">&nbsp;</span></p>
			</td>
		</tr>
	</tbody>
</table>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">&nbsp;</span></h4>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="color:#FFF0F5"><span style="background:rgb(153, 153, 153) none repeat scroll 0% 0%; font-family:arial,sans-serif; font-size:13.5pt"><span style="background-color:rgb(255, 0, 0)">Not Applicable States / UTs</span></span></span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-padding-alt:0in 5.4pt 0in 5.4pt; mso-yfti-tbllook:1184; width:706.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
			<td valign="bottom" style="width:206.75pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="276" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Central</span></p>
			</td><td valign="bottom" style="width:157.5pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="210" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Dadra and Nagar Haveli</span></p>
			</td><td valign="bottom" style="width:148.5pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="198" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Himachal Pradesh</span></p>
			</td><td valign="bottom" style="width:193.5pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="258" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Uttar Pradesh</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1;height:15.0pt">
			<td valign="bottom" style="width:206.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="276" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Andaman and Nicobar Islands</span></p>
			</td><td valign="bottom" style="width:157.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="210" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Daman and Diu</span></p>
			</td><td valign="bottom" style="width:148.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="198" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Jammu and Kashmir</span></p>
			</td><td valign="bottom" style="width:193.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="258" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Uttarakhand</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2;height:15.0pt">
			<td valign="bottom" style="width:206.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="276" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Arunachal Pradesh</span></p>
			</td><td valign="bottom" style="width:157.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="210" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Delhi</span></p>
			</td><td valign="bottom" style="width:148.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="198" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Ladakh</span></p>
			</td><td valign="bottom" style="width:193.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="258" nowrap>&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:3;height:15.0pt">
			<td valign="bottom" style="width:206.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="276" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Chandigarh</span></p>
			</td><td valign="bottom" style="width:157.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="210" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Goa</span></p>
			</td><td valign="bottom" style="width:148.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="198" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Lakshadweep</span></p>
			</td><td valign="bottom" style="width:193.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="258" nowrap>&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:15.0pt">
			<td valign="bottom" style="width:206.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="276" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Chhattisgarh</span></p>
			</td><td valign="bottom" style="width:157.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="210" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Haryana</span></p>
			</td><td valign="bottom" style="width:148.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="198" nowrap>
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; mso-ascii-font-family:Calibri; mso-bidi-font-family:Calibri; mso-bidi-font-size:11.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-hansi-font-family:Calibri">Rajasthan</span></p>
			</td><td valign="bottom" style="width:193.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt" width="258" nowrap>&nbsp;</td>
		</tr>
	</tbody>
</table>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">&nbsp;</span></h4>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Andhra Pradesh</span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Andhra Pradesh Tax On Professions, Trades, Callings And Employments Act, 1987</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Andhra Pradesh Tax On Professions, Trades, Callings And Employment Rules, 1987</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM No. V.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
    height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Memorandum Of Association</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Articles Of Association</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PAN Card</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Lease Agreement</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Board resolution for authorized signatory</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">No of directors and Photos and Proof of Residence of proprietor, Managing Partner, Managing Director, authorised person [Aadhaar]</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">No of employees</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration fees of Rs 2,500 per director</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Bank Details with Cancel Cheque</span></p>

						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l4 level1 lfo2;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Sale deed / Rental agreement / Lease Deed / No objection certificate in case of Rent Free for&nbsp; "Additional places of Business" (1st &amp; last pages)</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1"><a target="_blank" href="https://www.apct.gov.in/apportal/AllActs/APPT/APPTAct.aspx"><span style="color:black; mso-themecolor:text1">https://www.apct.gov.in/apportal/AllActs/APPT/APPTAct.aspx</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Upto 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 15001 To 20000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Above 20001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Monthly PT Remittance And Filing Of Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every month on day 10</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM No. V.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Assam</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Assam Professions, Trades, Callings And Employments Taxation Act, 1947</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Assam Professions, Trades, Callings And Employments Taxation Rules, 1947</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM III.xlsx<br />
			FORM IIIA.xlsx<br />
			FORM VII C.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">VAT Registration copy</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">No of employees</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:12.0pt; mso-fareast-font-family:Symbol; mso-fareast-language:EN-IN; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://www.tax.assam.gov.in/AssamTimsInfo/"><span style="color:black; mso-themecolor:text1">http://www.tax.assam.gov.in/AssamTimsInfo/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 10000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 10001 To 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 15001 To 25000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 180.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 25001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 208.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Remittance And Monthly Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on day 28</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM III.xlsx<br />
			FORM IIIA.xlsx<br />
			FORM VII C.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<h4 style="margin:0in;margin-bottom:.0001pt">
<br style="mso-special-character:line-break" />
&nbsp;</h4>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:black; font-family:arial,sans-serif; font-size:13.5pt; font-weight:normal; mso-themecolor:text1">&nbsp;</span></h4>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Bihar</span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid #DDDDDD .75pt; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Bihar Tax On Professions, Trades, Callings And Employments Act, 2011</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Bihar Professional Tax Rules, 2011</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM PT-IV.xlsx<br />
			FORM PT-V.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Memorandum Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Articles Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PAN Card</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Lease Agreement</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Employee list with salary details</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">ID &amp; Address Proof of Proprietor or Organisation</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Bank Account details</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l6 level1 lfo3;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Name &amp; Activities of the Firm or organisation</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1"><a target="_blank" href="https://www.biharcommercialtax.gov.in/bweb/"><span style="color:black; mso-themecolor:text1">https://www.biharcommercialtax.gov.in/bweb/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Upto 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 300001 To 500000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 1000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 500001 To 1000000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 2000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Above 1000001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every year(s) on day 30 of November</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM PT-IX.xlsx<br />
			FORM PT-VIII.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="line-height:11.25pt" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:8.5pt; mso-themecolor:text1">Note: Every employer shall deduct the tax payable by every employee from the salary or wages payable to such employee in respect of the month of September every year</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Gujarat</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid #DDDDDD .75pt; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Gujarat Panchayats, Municipal Corporations And State Tax On Professions, Traders, Callings And Employments Act, 1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Gujarat State Tax On Professions, Traders, Callings And Employments Rules, 1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. 5.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Office tax bill</span></p>

			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
    height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Owner ID proof</span></p>

						<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
							<tbody>
								<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
      height:15.0pt">
									<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
									<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rent Agreement</span></p>

									<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Two Passport size photo of owner</span></p>

									<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
										<tbody>
											<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
        height:15.0pt">
												<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
												<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">LWF paid receipt</span></p>

												<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
													<tbody>
														<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
          yes;height:15.0pt">
															<td style="width:279.0pt;padding:0in 0in 0in 0in;
          height:15.0pt" width="372">
															<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Owner Tax bill &amp; receipt</span></p>

															<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
																<tbody>
																	<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
            yes;height:15.0pt">
																		<td style="width:279.0pt;padding:0in 0in 0in 0in;
            height:15.0pt" width="372">
																		<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association, Articles Of Association and Incorporation Certificate</span></p>

																		<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
																			<tbody>
																				<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
              yes;height:15.0pt">
																					<td style="width:279.0pt;padding:0in 0in 0in 0in;
              height:15.0pt" width="372">
																					<p style="margin-bottom:6.75pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Company PAN card copy</span></p>

																					<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
																						<tbody>
																							<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
                yes;height:15.0pt">
																								<td style="width:279.0pt;padding:0in 0in 0in 0in;
                height:15.0pt" width="372">
																								<p style="margin-bottom:6.75pt;line-height:
                normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Cancelled Letterhead</span></p>

																								<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
																									<tbody>
																										<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
                  yes;height:15.0pt">
																											<td style="width:279.0pt;padding:0in 0in 0in 0in;
                  height:15.0pt" width="372">
																											<p style="margin-bottom:6.75pt;line-height:
                  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Authorisation letter</span></p>

																											<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
																												<tbody>
																													<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:
                    yes;height:15.0pt">
																														<td style="width:279.0pt;padding:0in 0in 0in 0in;
                    height:15.0pt" width="372">
																														<p style="margin-bottom:0in;margin-bottom:
                    .0001pt;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">All documents with stamp and sign by Owner / Manager</span></p>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		</td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://commercialtax.gujarat.gov.in/"><span style="color:black; mso-themecolor:text1">https://commercialtax.gujarat.gov.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 2999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 3000 To 5999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 6000 To 8999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 80.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 9000 To 11999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 12000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 20 of December</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. 5A.xlsx<br />
			FORM No. 5AA.xlsx<br />
			FORM No. 5C.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Monthly Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on day 15</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. 5.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Jharkhand</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Jharkhand Tax On Profession, Trades, Callings And Employments Act, 2011</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Jharkhand State Tax On Professions, Trades, Callings And Employments Rules, 2012</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM JPT201.xlsx<br />
			FORM JPT202.xlsx<br />
			FORM JPT204.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Three passport size photos</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://jharkhandcomtax.gov.in/professional-tax-acts"><span style="color:black; mso-themecolor:text1">http://jharkhandcomtax.gov.in/professional-tax-acts</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 300001 To 500000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 500001 To 800000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1800.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 800001 To 1000000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2100.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 1000001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 31 of October</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM JPT203.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  11.25pt" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:8.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Note: Salary and wage earners, such persons whose annual salaries or wages will be considered for deduction</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Karnataka</span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Karnataka Tax On Professions, Trades, Callings And Employments Act,1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Karnataka Tax On Professions, Trades, Callings And Employments Rules,1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person above 60 Years of age, tax is not required to be paid under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM 9-A.docx<br />
			FORM 5-A.docx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form 1 and Form II Application forms</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Memorandum Of Association &amp; Incorporation Certificate</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Article Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PAN Card</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Lease Agreement</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Bank Details with Cancel Cheque</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Employer Address Proof and ID Proof</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Email address and Phone Number</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">INR 2500/- for Enrollement Fees</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Two Photo Graphs of the Employer</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l8 level1 lfo4;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List of employees with Gross Salary</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1"><a target="_blank" href="https://vat.kar.nic.in/epay/"><span style="color:black; mso-themecolor:text1">https://vat.kar.nic.in/epay/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Above 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every year(s) on day 30 of April</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM 5.docx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Monthly PT Remittance And Filing Of Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every month on day 20</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM 9-A.docx<br />
			FORM 5-A.docx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Kerala</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Kerala Panchayat Raj Act, 1994</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Kerala Panchayat Raj (Profession Tax) Rules, 1996</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employee list with salary details</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:6;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List of Board of Directors</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://www.lsgkerala.gov.in/"><span style="color:black; mso-themecolor:text1">http://www.lsgkerala.gov.in</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 11999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 12000 To 17999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 120.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 18000 To 29999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 180.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 30000 To 44999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 300.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 45000 To 59999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 450.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 60000 To 74999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 600.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 75000 To 99999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 750.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 100000 To 124999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 125000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1250.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Madhya Pradesh</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Madhya Pradesh Vritti Kar Adhiniyam, 1995</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Madhya Pradesh Vritti Kar Niyam, 1995</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Bank Account details</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Name &amp; Activities of the Firm or organisation</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:6;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:7;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employee list with salary details</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://mptax.mp.gov.in/mpvatweb/"><span style="color:black; mso-themecolor:text1">https://mptax.mp.gov.in/mpvatweb/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 225000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 225001 To 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 125 Per Month</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 300001 To 400000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 166 Per Month For 11 Months And INR 174 For 12th Month</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 400001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 208 Per Month For 11 Months And INR 212 For 12th Month</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Monthly PT Remittance</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on day 10</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Quarterly Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every 3 month(s) starting on day 15 of January</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM 7.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Maharashtra</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid #DDDDDD .75pt; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Maharashtra State Tax On Professions, Trade, Callings And Employments Act, 1975</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Maharashtra State Tax On Professions, Trades, Callings And Employments Rules, 1975</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person above 65 Years of age, tax is not required to be paid under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association and Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Bank Details with Cancel Cheque</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:6;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Email address and Phone Number</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:7;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">2500/- for Enrollment Fees</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:8;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List of employees with Gross Salary</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://mahavat.gov.in/Mahavat/GetDataAction?option=Profession%20Tax%20Act"><span style="color:black; mso-themecolor:text1">http://mahavat.gov.in/Mahavat/GetDataAction?option=Profession%20Tax%20Act</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 7500</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 7501 To 10000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 175.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil for Women</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 10001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 10001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 300.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer has to deduct on February month only</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 31 of March</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:white;padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Monthly Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:white;padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on last day</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><br style="mso-special-character:line-break" />
			&nbsp;</p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Manipur</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Manipur Professions, Trades, Callings And Employments Taxation Act, 1981</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Manipur Professions, Trades, Callings And Employments Taxation Rules, 1982</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://manipurvat.gov.in/"><span style="color:black; mso-themecolor:text1">http://manipurvat.gov.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 50000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 50001 To 75000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 75001 To 100000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 100001 To 125000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2400.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 125001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 30 of March</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Meghalaya</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Meghalaya Professions, Trades, Callings And Employments Taxation Act, 1947</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Meghalaya Professions, Trades, Callings And Employments Taxation Rules, 1947</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM III.xlsx<br />
			FORM IIIA.xlsx<br />
			FORM VIIC.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association and Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">S&amp;E Certificate / Trade License Copy</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://megvat.gov.in/"><span style="color:black; mso-themecolor:text1">http://megvat.gov.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 50000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 50001 To 75000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 75001 To 100000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 300.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 100001 To 150000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 150001 To 200000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 750.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 200001 To 250000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 250001 To 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1250.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 300001 To 350000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 350001 To 400000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1800.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 400001 To 450000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2100.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:21">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 450001 To 500000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2400.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:22">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 500001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:23">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:24">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:25">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:26;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Remittance And Monthly Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on day 28</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM III.xlsx<br />
			FORM IIIA.xlsx<br />
			FORM VIIC.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Mizoram</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid #DDDDDD .75pt; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Mizoram Professions, Trades, Callings and Employments Taxation Act, 1995</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Mizoram Professions, Trades, Callings and Employments Taxation Rules, 1996</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM I.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://zotax.nic.in/"><span style="color:black; mso-themecolor:text1">http://zotax.nic.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 5000</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 5001 To 8000</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 75.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Assesee may pay in lump sum INR 900 per annum</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 8001 To 10000</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 120.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Assesee may pay in lump sum INR 1440 per annum</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 10001 To 12000</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Assesee may pay in lump sum INR 1800 per annum</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 12001 To 15000</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 180.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Assesee may pay in lump sum INR 2160 per annum</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 15001</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 208.00</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Assesee may pay in lump sum INR 2500 per annum</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="width:231.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="309">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 30 of June</span></p>
			</td><td valign="top" style="width:295.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="393">
			<p align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM I.xlsx</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Nagaland</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Nagaland Professions, Trades, Callings And Employments Taxation Act, 1968</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Nagaland Professions, Trades, Callings And Employments Taxation Rules, 1970</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memmorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://www.nagalandtax.nic.in/"><span style="color:black; mso-themecolor:text1">http://www.nagalandtax.nic.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 4000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 4001 To 5000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 35.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 5001 To 7000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 75.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 7001 To 9000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 110.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 9001 To 12000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 180.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 12001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 208.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 30 of April</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM V.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Odisha</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Orissa State Tax On Professions, Trades, Callings And Employments Act, 2000</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Orissa State Tax On Professions, Trades, Callings And Employments Rules, 2000</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. V.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">S&amp;E Certificate / Trade License Copy</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:6;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Email address and Phone Number</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:7;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 2500/- for Enrollment Fees</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:8;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Bank Details with Cancel Cheque</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:9;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List of employees with Gross Salary</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://odishatax.gov.in/"><span style="color:black; mso-themecolor:text1">https://odishatax.gov.in/#</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 160000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 160001 To 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 300001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Consolidated Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 30 of March</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. V.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Monthly Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every month on last day</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM No. V.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:19;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  11.25pt" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:8.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Note: Salary and wage earners, such persons whose annual salaries or wages will be considered for deduction</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Puducherry</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Puducherry Village And Commune Panchayats Act, 1973, The Puducherry Municipalities Act, 1973</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Puducherry Village And Commune Panchayats (Profession Tax) Rules, 1976, Puducherry Municipalities (Profession Tax) Rules, 1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association &amp; Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Shops and Establishment Registration Certificate / Trade License Copy</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://law.puducherry.gov.in/code3.pdf"><span style="color:black; mso-themecolor:text1">http://law.puducherry.gov.in/code3.pdf</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 99999</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 100000 To 200000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 250.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 200001 To 300000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 500.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 300001 To 400000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 750.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 400001 To 500000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1000.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 500001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1250.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Half Yearly Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every 6 month(s) starting on last day of June</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Punjab</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Punjab State Development Tax Act, 2018</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Punjab State Development Tax Rules, 2018</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">All such persons who are assessable under the Head Income from Salaries and/ or Wages as per the Income Tax Act, 1961</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">NA</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM PSDT-8.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://www.pextax.com/PEXWAR/appmanager/pexportal/PunjabExcise"><span style="color:black; mso-themecolor:text1">https://www.pextax.com/PEXWAR/appmanager/pexportal/PunjabExcise</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 250000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">All such persons who are assessable under the Head Income from Salaries and/ or Wages as per the Income Tax Act, 1961</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  11.25pt" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:8.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Note: All such persons who are assessable under the Head Income from Salaries and/ or Wages as per the Income Tax Act, 1961</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Sikkim</span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Sikkim Tax On Professions, Trades, Callings And Employments Act, 2006</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Sikkim Tax On Professions, Trades, Callings And Employments Rules, 2006</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="background:white; border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l5 level1 lfo5;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Memorandum Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l1 level1 lfo6;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l2 level1 lfo7;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin:0in;margin-bottom:.0001pt;text-indent:
    -.25in;line-height:normal;mso-list:l7 level1 lfo8;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1"><a target="_blank" href="http://www.sikkimtax.gov.in/"><span style="color:black; mso-themecolor:text1">http://www.sikkimtax.gov.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Upto 20000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 20001 To 30000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 125.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 30001 To 40000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Above 40001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Quarterly Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every 3 month(s) starting on day 30 of April</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM 5.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Tamil Nadu</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Tamil Nadu Municipal Laws (Second Amendment) Act, 1998</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Tamil Nadu Professional Tax Rules, 1999</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM 1.docx<br />
			FORM 3.docx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association &amp; Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Shops and Establishment Registration Certificate / Trade License Copy</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://erp.chennaicorporation.gov.in/e-portal/login.do"><span style="color:black; mso-themecolor:text1">https://erp.chennaicorporation.gov.in/e-portal/login.do</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 21000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377;</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 21001 To 30000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 135.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 30001 To 45000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 315.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 45001 To 60000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 690.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 60001 To 75000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1025.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 75001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1250.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:20">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Payment Of Enrollment Tax</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every 6 month(s) starting on last day of September</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM 2.docx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:21;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  11.25pt" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:8.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Note: PT Slab For Greater Chennai Corporation</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<h4 style="margin:0in;margin-bottom:.0001pt"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-bidi-font-weight:normal; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Telangana</span></h4>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid #DDDDDD .75pt; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Telangana Tax On Professions, Trades, Callings And Employments Act, 1987</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">The Telangana Tax On Professions, Trades, Callings And Employment Rules, 1987</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM V.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Memorandum Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Articles Of Association</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">PAN Card</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Lease Agreement</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Employer Address Proof and ID Proof and photos</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Board resolution for authorized signatory</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">No of directors and Photos and Proof of Residence of proprietor, Managing Partner, Managing Director, authorised person [Aadhaar]</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">No of employees</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Registration fees of Rs 2,500 per director</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Bank Details with Cancel Cheque</span></p>

			<p style="margin:0in;margin-bottom:.0001pt;text-indent:-.25in;
  line-height:normal;mso-list:l3 level1 lfo9;tab-stops:list .5in" class="MsoNormal"><span style="color:black; font-family:symbol; font-size:10.0pt; mso-bidi-font-family:Symbol; mso-bidi-font-size:10.5pt; mso-fareast-font-family:Symbol; mso-themecolor:text1"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt \"Times New Roman\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="color:black; font-family:arial,sans-serif; font-size:10.5pt; mso-themecolor:text1">Sale deed / Rental agreement / Lease Deed / No objection certificate in case of Rent Free for&nbsp; "Additional places of Business" (1st &amp; last pages)</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1"><a target="_blank" href="https://simpliance.in/professional-tax-detail/www.tgct.gov.in"><span style="color:black; mso-themecolor:text1">www.tgct.gov.in</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Upto 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Between 15001 To 20000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Above 20001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="4">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Form</span></strong></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Monthly PT Remittance And Filing Of Return</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; mso-themecolor:text1">Every month on day 10</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; line-height:107%; mso-themecolor:text1">FORM V.xlsx</span></p>
			</td><td style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt .75pt .75pt">&nbsp;</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">Tripura</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Tripura Professions, Trades, Callings And Employments Taxation Act, 1997</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The Tripura Professions, Trades, Callings And Employments Taxation Rules, 1998</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM VII.xlsx<br />
			FORM VIII.xlsx</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Offline</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association and Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Shops and Establishment Registration Certificate / Trade License Copy</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="https://taxes.tripura.gov.in/professional-tax1.asp"><span style="color:black; mso-themecolor:text1">https://taxes.tripura.gov.in/professional-tax1.asp</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 7500</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 7501 To 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 1800.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 150 Per Month</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 15001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 2496.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 208 Per Month</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p class="MsoNormal"><span style="color:black; mso-themecolor:text1">&nbsp;</span></p>

<p style="margin-bottom:7.5pt;line-height:normal;mso-outline-level:
4" class="MsoNormal"><strong style="mso-bidi-font-weight:normal"><span style="background:#999999; color:white; font-family:arial,sans-serif; font-size:13.5pt; mso-effects-shadow-align:topleft; mso-effects-shadow-alpha:100.0%; mso-effects-shadow-angledirection:2700000; mso-effects-shadow-anglekx:0; mso-effects-shadow-angleky:0; mso-effects-shadow-color:#ED7D31; mso-effects-shadow-dpidistance:3.0pt; mso-effects-shadow-dpiradius:0pt; mso-effects-shadow-pctsx:100.0%; mso-effects-shadow-pctsy:100.0%; mso-effects-shadow-themecolor:accent2; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-style-textoutline-fill-alpha:100.0%; mso-style-textoutline-fill-color:#ED7D31; mso-style-textoutline-fill-themecolor:accent2; mso-style-textoutline-outlinestyle-align:center; mso-style-textoutline-outlinestyle-compound:simple; mso-style-textoutline-outlinestyle-dash:solid; mso-style-textoutline-outlinestyle-dpiwidth:.52pt; mso-style-textoutline-outlinestyle-join:round; mso-style-textoutline-outlinestyle-linecap:flat; mso-style-textoutline-outlinestyle-pctmiterlimit:0%; mso-style-textoutline-type:solid">West Bengal</span></strong></p>

<table cellpadding="0" border="1" style="border-collapse:collapse; border:none; mso-border-alt:solid windowtext .5pt; mso-border-insideh:.5pt solid windowtext; mso-border-insidev:.5pt solid windowtext; mso-yfti-tbllook:1184; width:704.25pt" cellspacing="0" class="MsoNormalTable">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Act</span></strong></p>
			</td><td valign="top" style="border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The West Bengal State Tax On Professions, Trades, Callings And Employments Act, 1979</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Rule</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">The West Bengal State Tax On Professions, Trades,Callings And Employments Rules,1976</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Applicability</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">A person or employer by whom tax is payable under this Act</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Exemption</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Not Applicable</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Form</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:5">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Registration Process</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Online</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:6">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List Of Documents For Registration</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<table cellpadding="0" border="0" style="border-collapse:collapse; mso-padding-alt:0in 0in 0in 0in; mso-yfti-tbllook:1184; width:279.0pt" cellspacing="0" class="MsoNormalTable">
				<tbody>
					<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.0pt">
						<td style="width:279.0pt;padding:0in 0in 0in 0in;height:15.0pt" width="372">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Memorandum Of Association &amp; Incorporation Certificate</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:1;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Articles Of Association</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:2;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PAN Card</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:3;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Lease Agreement</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:4;height:.25in">
						<td style="padding:0in 0in 0in 0in;height:.25in">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">S&amp;E Certificate / Trade License Copy</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:5;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Employer Address Proof and ID Proof</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:6;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Email address and Phone Number</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:7;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">INR 2500/- for Enrollment Fees</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:8;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Bank Details with Cancelled Cheque</span></p>
						</td>
					</tr>
					<tr style="mso-yfti-irow:9;mso-yfti-lastrow:yes;height:15.0pt">
						<td style="padding:0in 0in 0in 0in;height:15.0pt">
						<p style="margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">List of employees with Gross Salary</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="mso-yfti-irow:7">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Website</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="2">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1"><a target="_blank" href="http://wbprofessiontax.gov.in/"><span style="color:black; mso-themecolor:text1">http://wbprofessiontax.gov.in/</span></a></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:8">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:9">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Professional Rates</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:10">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Salary (INR)</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">PT Amount</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Remarks</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:11">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Upto 8500</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:12">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 8501 To 10000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 0.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Nil</span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:13">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 10001 To 15000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 110.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:14">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 15001 To 25000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 130.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:15">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Between 25001 To 40000</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 150.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:16">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Above 40001</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">&#8377; 200.00</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:17">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F3F3F3;padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">&nbsp;</td>
		</tr>
		<tr style="mso-yfti-irow:18">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt" colspan="3">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Filing Of Returns</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:19">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Task</span></strong></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Last Date Of Filing Returns</span></strong></p>
			</td><td valign="top" style="width:194.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="260">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><strong><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Form</span></strong></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:20;mso-yfti-lastrow:yes">
			<td valign="top" style="border:solid windowtext 1.0pt;border-top:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Annual Returns</span></p>
			</td><td valign="top" style="border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:3.75pt 7.5pt 3.75pt 7.5pt">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:12.0pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">Every year(s) on day 30 of April</span></p>
			</td><td valign="top" style="width:194.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:3.75pt 7.5pt 3.75pt 7.5pt" width="260">
			<p style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal" class="MsoNormal"><span style="color:black; font-family:arial,sans-serif; font-size:9.5pt; mso-fareast-font-family:\"Times New Roman\"; mso-fareast-language:EN-IN; mso-themecolor:text1">FORM III.xlsx</span></p>
			</td>
		</tr>
	</tbody>
</table>
