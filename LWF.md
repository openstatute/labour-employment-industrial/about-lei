<table cellpadding="0" border="0" style="height:346px; width:994px" cellspacing="0">
    <colgroup>
        <col />
        <col />
        <col />
        <col />
    </colgroup>
    <tbody>
        <tr height="20">
            <td height="20" style="height: 20px; width: 988px; text-align: center;" colspan="4">
                <span style="color:#FFFFFF">
                    <span style="font-family:comic sans ms,cursive">
                        <span style="font-size:24px">
                            <em>
                                <u>
                                    <strong>
                                        <span style="background-color:rgb(0, 0, 255)">LWF Applicable States</span>
                                    </strong>
                                </u>
                            </em>
                        </span>
                    </span>
                </span>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Andhra Pradesh">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Andhra Pradesh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Goa">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Goa</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Kerala">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Kerala</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Punjab">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Punjab</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Chandigarh">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Chandigarh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Gujarat">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Gujarat</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Madhya Pradesh">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Madhya Pradesh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Tamil Nadu">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Tamil Nadu</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Chhattisgarh">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Chhattisgarh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Haryana">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Haryana</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Maharashtra">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Maharashtra</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Telangana">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Telangana</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Delhi">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Delhi</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Karnataka">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Karnataka</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Odisha">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Odisha</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="West Bengal">
                    <span style="color:#008000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">West Bengal</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">&nbsp;</td>
            <td style="width: 337px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width: 204px;">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; text-align: center; width: 988px;" colspan="4">
                <span style="color:#FFFFFF">
                    <span style="font-family:comic sans ms,cursive">
                        <span style="font-size:24px">
                            <u>
                                <em>
                                    <strong>
                                        <span style="background-color:rgb(0, 0, 255)">LWF Not Applicable States</span>
                                    </strong>
                                </em>
                            </u>
                        </span>
                    </span>
                </span>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Central">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Central</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Dadra and Nagar Haveli">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Dadra and Nagar Haveli</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Lakshadweep">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Lakshadweep</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Pondicherry">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Pondicherry</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Andaman and Nicobar Islands">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Andaman and Nicobar Islands</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Daman and Diu">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Daman and Diu</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Manipur">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Manipur</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Rajasthan">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Rajasthan</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Arunachal Pradesh">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Arunachal Pradesh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Himachal Pradesh">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Himachal Pradesh</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Meghalaya">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Meghalaya</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Sikkim">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Sikkim</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Assam">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Assam</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Jammu and Kashmir">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Jammu and Kashmir</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Mizoram">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Mizoram</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Tripura">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Tripura</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 258px;">
                <div title="Bihar">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Bihar</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 337px;">
                <div title="Jharkhand">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Jharkhand</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td>
                <div title="Nagaland">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Nagaland</span>
                        </strong>
                    </span>
                </div>
            </td>
            <td style="width: 204px;">
                <div title="Uttar Pradesh">
                    <span style="color:#FF0000">
                        <strong>
                            <span style="font-family:comic sans ms,cursive">Uttar Pradesh</span>
                        </strong>
                    </span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="0" border="0" style="height:7206px; width:1100px" cellspacing="0">
    <colgroup>
        <col />
        <col />
        <col />
        <col />
        <col />
        <col />
    </colgroup>
    <tbody>
        <tr height="21">
            <td height="21" style="height:21px;width:313px;">
                <p>
                    <span style="color:#FFFFFF">
                        <span style="font-size:32px">
                            <strong>
                                <span style="background-color:rgb(0, 0, 255)">Andhra Pradesh</span>
                            </strong>
                        </span>
                    </span>
                </p>
            </td>
            <td style="width:155px;">&nbsp;</td>
            <td style="width:151px;">&nbsp;</td>
            <td style="width:123px;">&nbsp;</td>
            <td style="width:136px;">&nbsp;</td>
            <td style="width:179px;">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">Andhra Pradesh Labour Welfare Fund Act, 1987</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">Andhra Pradesh Labour Welfare Fund Rules, 1988</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM F.xlsx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://labour.ap.gov.in/ELabour/Views/RecentAmendementNewActs.aspx">http://labour.ap.gov.in/ELabour/Views/RecentAmendementNewActs.aspx</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="60">
            <td height="60" style="height:60px;width:313px;">All employees except those employed mainly in a managerial capacity or who is employed as an apprentice or on part-time basis</td>
            <td style="width: 155px; text-align: center;">30</td>
            <td style="width: 151px; text-align: center;">70</td>
            <td style="width: 123px; text-align: center;">100</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">31st January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">
                <div title="Central">
                    <div title="Central">&nbsp;</div>
                </div>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">
                <div title="Andaman and Nicobar Islands">
                    <div title="Andaman and Nicobar Islands">&nbsp;</div>
                </div>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Chandigarh</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Act,1965</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Rules,1966</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Monthly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://chandigarh.gov.in/dept_labour.htm">http://chandigarh.gov.in/dept_labour.htm</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Employee drawing salary upto Rupees Fifteen Thousand per month</td>
            <td rowspan="2" style="width: 155px; text-align: center;">5</td>
            <td rowspan="2" style="width: 151px; text-align: center;">20</td>
            <td rowspan="2" style="width: 123px; text-align: center;">25</td>
            <td rowspan="2" style="width: 136px; text-align: center;">Last Day Of Month</td>
            <td style="width: 179px; text-align: center;">15th October</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 179px; text-align: center;">15th April</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">
                <div title="Rajasthan">
                    <div title="Rajasthan">&nbsp;</div>
                </div>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Chhattisgarh</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Chhattisgarh Shram Kalyan Nidhi Adhiniyam, 1982</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Chhattisgarh Pradesh Shram Kalyan Nidhi Rules, 1984</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://cglabour.nic.in/ShramKalyanMandal/abhidayrashi.aspx">http://cglabour.nic.in/ShramKalyanMandal/abhidayrashi.aspx</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or administrative capacity or employed in a supervisory capacity drawing wages exceeding ten thousand per month</td>
            <td style="width: 155px; text-align: center;">15</td>
            <td style="width: 151px; text-align: center;">45</td>
            <td style="width: 123px; text-align: center;">60</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or administrative capacity or employed in a supervisory capacity drawing wages exceeding ten thousand per month</td>
            <td style="width: 155px; text-align: center;">15</td>
            <td style="width: 151px; text-align: center;">45</td>
            <td style="width: 123px; text-align: center;">60</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Delhi</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Bombay Labour Welfare Fund Act, 1953</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Delhi Labour Welfare Fund Rules, 1997</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing five or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://www.delhi.gov.in/wps/wcm/connect/DOIT_Labour/labour/notification/labour+welfare+funds">http://www.delhi.gov.in/wps/wcm/connect/DOIT_Labour/labour/notification/labour+welfare+funds</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Two thousand Five Hundred only per month</td>
            <td style="width: 155px; text-align: center;">0.75</td>
            <td style="width: 151px; text-align: center;">2.25</td>
            <td style="width: 123px; text-align: center;">3</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Two thousand Five Hundred only per month</td>
            <td style="width: 155px; text-align: center;">0.75</td>
            <td style="width: 151px; text-align: center;">2.25</td>
            <td style="width: 123px; text-align: center;">3</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Goa</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Goa Labour Welfare Fund Act, 1986</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">Goa Labour Welfare Fund, Rules 1990</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A.xlsx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://labour.goa.gov.in/index.php/goa-lwb/">http://labour.goa.gov.in/index.php/goa-lwb/</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or supervisory capacity and drawing wages exceeding rupees one thousand six hundred only per month</td>
            <td style="width: 155px; text-align: center;">60</td>
            <td style="width: 151px; text-align: center;">180</td>
            <td style="width: 123px; text-align: center;">240</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">31st July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or supervisory capacity and drawing wages exceeding rupees one thousand six hundred only per month</td>
            <td style="width: 155px; text-align: center;">60</td>
            <td style="width: 151px; text-align: center;">180</td>
            <td style="width: 123px; text-align: center;">240</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">31st January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Gujarat</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Gujarat Labour Welfare Fund Act, 1953</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Labour Welfare Fund (Gujarat) Rules, 1962</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing ten or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A-1.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="https://glwb.gujarat.gov.in/">https://glwb.gujarat.gov.in/</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or supervisory capacity and drawing wages exceeding rupees three thousand and five hundred per month</td>
            <td style="width: 155px; text-align: center;">6</td>
            <td style="width: 151px; text-align: center;">12</td>
            <td style="width: 123px; text-align: center;">18</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">31st July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in managerial or supervisory capacity and drawing wages exceeding rupees three thousand and five hundred per month</td>
            <td style="width: 155px; text-align: center;">6</td>
            <td style="width: 151px; text-align: center;">12</td>
            <td style="width: 123px; text-align: center;">18</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">31st January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Haryana</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Act,1965</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Rules,1966</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing ten or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Monthly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="https://hrylabour.gov.in/content/cms/MTU">https://hrylabour.gov.in/content/cms/MTU</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td rowspan="3" height="140" style="height:140px;width:313px;">All employees employed, directly by or through any agency (including a contractor) with or without the knowledge of the principal employer, for remuneration in any factory or establishment to do any work connected with its affairs. Each employee shall contribute to the Fund every month an amount equal to zero point two percent of his salary or wages or any remuneration subject to a limit of rupees twenty-five and each employer in respect of each such employee shall contribute to the Fund every month, twice the amount contributed by such employee</td>
            <td rowspan="3" style="width: 155px; text-align: center;">25</td>
            <td rowspan="3" style="width: 151px; text-align: center;">50</td>
            <td rowspan="3" style="width: 123px; text-align: center;">75</td>
            <td style="width: 136px; text-align: center;">Every Month (Also being accepted Quarterly</td>
            <td style="width: 179px; text-align: center;">Last Day Of The Month (Also being accepted Quarterly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Half Yearly</td>
            <td style="width: 179px; text-align: center;">Half Yearly</td>
        </tr>
        <tr height="40">
            <td height="40" style="height: 40px; width: 136px; text-align: center;">Yearly in certain cases)</td>
            <td style="width: 179px; text-align: center;">Yearly in certain cases)</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Karnataka</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Karnataka Labour Welfare Fund Act, 1965</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Karnataka Labour Welfare Fund Rules, 1968</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing fifty or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM D.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://labour.kar.nic.in/labour/klwboard.htm">http://labour.kar.nic.in/labour/klwboard.htm</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="60">
            <td height="60" style="height:60px;width:313px;">All employees who are employed for wages to do any work skilled or unskilled, manual or clerical, in an establishment</td>
            <td style="width: 155px; text-align: center;">20</td>
            <td style="width: 151px; text-align: center;">40</td>
            <td style="width: 123px; text-align: center;">60</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Kerala</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Kerala Shops And Commercial Establishments Workers Welfare Fund Act,2006</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Kerala Shops And Commercial Establishments Workers Welfare Fund Scheme, 2007</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Monthly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM 6.docx&nbsp; Form - 5.xlsx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://peedika.kerala.gov.in/">http://peedika.kerala.gov.in/</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height: 40px; width: 313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="40">
            <td height="40" style="height: 40px; width: 313px;">All employees under the purview of the Kerala Shops and Commercial Establishments Act, 1960</td>
            <td style="width: 155px; text-align: center;">20</td>
            <td style="width: 151px; text-align: center;">20</td>
            <td style="width: 123px; text-align: center;">40</td>
            <td style="width: 136px; text-align: center;">Monthly</td>
            <td style="width: 179px; text-align: center;">5th Of Every Month</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="font-size:32px">
                    <span style="color:#FFFFFF">
                        <span style="background-color:rgb(0, 0, 255)">Madhya Pradesh</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Madhya Pradesh Shram Kalyan Nidhi Adhiniyam, 1982</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Madhya Pradesh Shram Kalyan Nidhi Rules, 1984</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing one or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://shramsewa.mp.gov.in/labourwelfareboard/en-us/">http://shramsewa.mp.gov.in/labourwelfareboard/en-us/</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Ten Thousand only per month</td>
            <td style="width: 155px; text-align: center;">10</td>
            <td style="width: 151px; text-align: center;">30</td>
            <td style="width: 123px; text-align: center;">40</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Ten Thousand only per month</td>
            <td style="width: 155px; text-align: center;">10</td>
            <td style="width: 151px; text-align: center;">30</td>
            <td style="width: 123px; text-align: center;">40</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Maharashtra</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Maharashtra Labour Welfare Fund Act, 1953</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Maharashtra Labour Welfare Fund Rules, 1953</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing five or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A-1.xls&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="https://mahakamgar.maharashtra.gov.in/index.htm">https://mahakamgar.maharashtra.gov.in/index.htm</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages upto Rupees Three Thousand only per month</td>
            <td style="width: 155px; text-align: center;">6</td>
            <td style="width: 151px; text-align: center;">18</td>
            <td style="width: 123px; text-align: center;">24</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages upto Rupees Three Thousand only per month</td>
            <td style="width: 155px; text-align: center;">6</td>
            <td style="width: 151px; text-align: center;">18</td>
            <td style="width: 123px; text-align: center;">24</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Three Thousand only per month</td>
            <td style="width: 155px; text-align: center;">12</td>
            <td style="width: 151px; text-align: center;">36</td>
            <td style="width: 123px; text-align: center;">48</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding Rupees Three Thousand only per month</td>
            <td style="width: 155px; text-align: center;">12</td>
            <td style="width: 151px; text-align: center;">36</td>
            <td style="width: 123px; text-align: center;">48</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Odisha</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Odisha Labour Welfare Fund Act, 1996</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Odisha Labour Welfare Fund Rules, 2015</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing twenty or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM F.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://www.labdirodisha.gov.in/">http://www.labdirodisha.gov.in</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="60">
            <td height="60" style="height:60px;width:313px;">All employees except those working in the managerial or supervisory capacity and who is employed as an apprentice or on part-time basis</td>
            <td style="width: 155px; text-align: center;">10</td>
            <td style="width: 151px; text-align: center;">20</td>
            <td style="width: 123px; text-align: center;">30</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="60">
            <td height="60" style="height:60px;width:313px;">All employees except those working in the managerial or supervisory capacity and who is employed as an apprentice or on part-time basis</td>
            <td style="width: 155px; text-align: center;">10</td>
            <td style="width: 151px; text-align: center;">20</td>
            <td style="width: 123px; text-align: center;">30</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">Punjab</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Act,1965</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Punjab Labour Welfare Fund Rules 1966</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing twenty or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Monthly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://pblabour.gov.in/">http://pblabour.gov.in/</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="40">
            <td rowspan="2" height="60" style="height:60px;width:313px;">Any person who is employed for hire or reward to do any work, skilled or unskilled, manual or clerical, in an establishment</td>
            <td rowspan="2" style="width: 155px; text-align: center;">5</td>
            <td rowspan="2" style="width: 151px; text-align: center;">20</td>
            <td rowspan="2" style="width: 123px; text-align: center;">25</td>
            <td rowspan="2" style="width: 136px; text-align: center;">Last Day Of Month</td>
            <td style="width: 179px; text-align: center;">15th April</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 179px; text-align: center;">15th October</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:#0000FF">Tamil Nadu</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The Tamil Nadu Labour Welfare Fund Act, 1972</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The Tamil Nadu Labour Welfare Fund Rules, 1973</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing five or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM A.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://www.labour.tn.gov.in/Labour/tnlabour.jsp">http://www.labour.tn.gov.in/Labour/tnlabour.jsp</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="100">
            <td height="100" style="height:100px;width:313px;">All employees except those working in managerial or supervisory capacity and drawing wages exceeding rupees three thousand five hundred per month or who is employed as an apprentice or on part-time basis</td>
            <td style="width: 155px; text-align: center;">10</td>
            <td style="width: 151px; text-align: center;">20</td>
            <td style="width: 123px; text-align: center;">30</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">31st January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:#0000FF">Telangana</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">Telangana Labour Welfare Fund Act, 1987</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">Telangana Labour Welfare Fund Rules, 1988</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing twenty or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM F.xlsx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="https://labour.telangana.gov.in/AboutUs.do">https://labour.telangana.gov.in/AboutUs.do</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All Employees except those working in the managerial or supervisory capacity and drawing wages exceeding rupees one thousand six hundred per month</td>
            <td style="width: 155px; text-align: center;">2</td>
            <td style="width: 151px; text-align: center;">5</td>
            <td style="width: 123px; text-align: center;">7</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">31st January</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="21">
            <td height="21" style="height:21px;">
                <span style="color:#FFFFFF">
                    <span style="font-size:32px">
                        <span style="background-color:rgb(0, 0, 255)">West Bengal</span>
                    </span>
                </span>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Act</td>
            <td style="width:743px;" colspan="5">The West Bengal Labour Welfare Fund Act, 1974</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Rule</td>
            <td style="width:743px;" colspan="5">The West Bengal Labour Welfare Fund Rules, 1976</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Applicability</td>
            <td style="width:743px;" colspan="5">Any Employer/Establishment employing ten or more employees/persons</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Frequency</td>
            <td style="width:743px;" colspan="5">Half Yearly</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Form</td>
            <td style="width:743px;" colspan="5">FORM D Statement Regarding Contributions.docx&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:313px;">Website</td>
            <td style="width:743px;" colspan="5">
                <a target="_blank" href="http://www.wblwb.org/html/index.php">http://www.wblwb.org/html/index.php</a>
            </td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">&nbsp;</td>
        </tr>
        <tr height="20">
            <td height="20" style="height:20px;width:1056px;" colspan="6">Labour Welfare Fund Contribution</td>
        </tr>
        <tr height="20">
            <td rowspan="2" height="40" style="height:40px;width:313px;">Category</td>
            <td rowspan="2" style="width: 155px; text-align: center;">Employee Contribution</td>
            <td rowspan="2" style="width: 151px; text-align: center;">Employer Contribution</td>
            <td rowspan="2" style="width: 123px; text-align: center;">Total Contribution</td>
            <td style="width: 136px; text-align: center;">Date Of</td>
            <td style="width: 179px; text-align: center;">Last Date For</td>
        </tr>
        <tr height="20">
            <td height="20" style="height: 20px; width: 136px; text-align: center;">Deduction</td>
            <td style="width: 179px; text-align: center;">Submission</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding rupees thousand six hundred per month</td>
            <td style="width: 155px; text-align: center;">3</td>
            <td style="width: 151px; text-align: center;">15</td>
            <td style="width: 123px; text-align: center;">18</td>
            <td style="width: 136px; text-align: center;">30th June</td>
            <td style="width: 179px; text-align: center;">15th July</td>
        </tr>
        <tr height="80">
            <td height="80" style="height:80px;width:313px;">All employees except those working in the managerial or supervisory capacity and drawing wages exceeding rupees thousand six hundred per month</td>
            <td style="width: 155px; text-align: center;">3</td>
            <td style="width: 151px; text-align: center;">15</td>
            <td style="width: 123px; text-align: center;">18</td>
            <td style="width: 136px; text-align: center;">31st December</td>
            <td style="width: 179px; text-align: center;">15th January</td>
        </tr>
    </tbody>
</table>
<p>NOTE: Last updated on 13th July' 2019</p>