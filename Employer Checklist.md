 
The Checklist For Employers
---------------------------

**Statutory Deposits,Returns and Notices**

**Month wise Checklist for Submission of Various Returns**

 

**Note: After being ONLINE system as the decision of the Head Quarter office of ESIC and Provident Fund not to be submits the entire returns for the act right now.**

**MONTH &  
DATE-WISE**

**NAME OF THE STATUE**

**FORM**

**NAME OF RETURN/COMPLIANCE**

**TO BE SENT TO**

**January**

 

 

 

 

15

The Employment Exchanges (CNV) Act,1959 & Rules

ER-I  
(Rule 6)

Quarterly Return for quarter  
ended

Local Employment Exchange

15

Gujarat/Delhi/Bombay/ Labour Welfare fund act,1953

Annexure "A"

Statement of contribution of along with chq.

Gujarat labour welfare fund Board/ Commissioner

21

The Maternity Benefit Act, 1961

LMNO  
Rule 16(1)

Annual Returns and details  
of payment ending 31st Dec.

Competent authority  
under the Act

30

The Contract Labour 9 (R&A) Act, 1970 & Rules

XXIV  
Rule 82(1)

Half yearly return by contractor  
(Indupllcate)

Concerned Licensing Officer

31

The Factories Act, 1948

 

 

Chief Inspector/Director  
or other competent  
authority of the area

 

**February**

 

 

 

 

1

The minimum Wages Act, 1948

III Rule 21 (4A)

Annual Return

Inspector under the are  
concerned

1

The Payment of Wages Act,1936 & related Mines Rules

V Rule 18 (related Mines Rules) VII, VIII Rule 16 related Air Transport

Annual Return

Concerned Labour  
Commissioner

 

 

 

 

Concerned Regional  
Commissioner

 

**April**

 

 

 

 

15

The Apprenticeship Act, 1961

APP-2

Half yearly return March ending

Concerned Regional Director/Apprenticeship Advisor

15

The Employment Exchange (CNV) Act, 1959 & Rules

ER-I Rule 6

Quarterly return for quarter ended 31 Dec.

Local Employment Exchange

30

The employees Provident fund act,1952

3A & 6A

Annual individual Returns & Returns of Contributions.

Regional PF Commissioner, SURAT.

 

**May**

 

 

 

 

12

The Employees State Insurance Act, 1948, Rules & Regulations

6 Sec.44 Regulation 26

Summary of contribution in quadruplicate a/w challans Monthly return a/w cheque

Respective local office or any Scheduled Bank Now it is online hence not submitted.

 

**July**

 

 

 

 

15

The Employment Exchanges (CNV) Act, 1959 & Rules

ER-I Rule 6

Quarterly return for the quarter ended

Concerned Employment Officer

15

The Factories Act, 1948

Refer to State Rules

Half yearly return

Concerned Director/Inspector

15

The Contract Labour (R&A) Act, 1970

XXIV

Half yearly return by contractor

Concerned Inspector

15

Gujarat/Delhi/Bombay Labour Welfare fund act

Annexere "A"

Statement of contribution of Jun. along with chq.

Gujarat labour welfare fund

 

**October**

 

 

 

 

15

The Apprenticeship Act, 1961

APP-2

Half yearly return Sept, ending

Dy. Apprenticeship Adviser

15

The Employment Exchanges (CNV) Act, 1959 & Rules

ER-I Rule 6

Quarterly return for the quarter ended

Concerned Employment Officer

30

The Factories Act, 1948 & Rules

Refer to State Rules

Application for renewal of licence

Director/Inspector of Factories

31

The Contract Labour (R&A)

VII Rule 29(2)

Application for renewal of licence

Concerned Inspector

 

**November**

 

 

 

 

12

The Employees State Insurance Act, 1948

6 Sec.44 Regulation 25

Summary of contribution in quadruplicate a/w challans

Concerned local office. Now it is online hence not submitted.

 

**December**

 

 

 

 

30

The Payment of Bonus Act, 1965 & Rules

D Rule 5

Annual Return - Bonus paid to the employees for the accounting year ending

Concerned inspector/under the Act

**Checklist for submission of Various Returns- Monthly/on Occurrence**

**TIME LIMIT**

**ACT**

**FORM ETC**

**NAME OF RETURN/COMPLIANCE**

**TO BE SENT TO**

21st of every month

The Employees State Insurance Act, 1948

Challans

Remittance of Contributions

Concerned Regional Office

Every month 15th

The Employees Provident Funds & MP Act, 1952

Challans

Remittance of Contributions

Concerned Regional Office

 

 

 

 

 

Immediately fatal/death & within 48 hrs. in ordinary cases and immediately in death or serious injury cases

The Employees State Insurance Act,1948

16

Accident Report

Concerned local office/dispensary of ESI

Every month

The Apprenticeship Act, 1961

Printed Format

Bill for reimbursement

Apprenticeship Advisor

4 Rule 14(4)

With seven days of joining of Apprentice

Regional Director/state Apprenticeship Advisor

1A Report containing record of basic training etc.Rule 14 (5&6)

Report for the period March-15th April ending Sept. 15th October

Regional Director/State Apprentices

3 Rule 14(7)

To be sent in Nov. & May Progress Report

 

6 Rule 8

Record of work done and the studies taken by Graduates Technicians etc.

Regional director/State apprenticeship Advisor

4 hours of occurrence

The Factories Act, 1948

Refer to State Rules

Notice of accident/dangerous occurrence

Inspector/Director of Factories

12 hours of occurrence

The Factories Act, 1948

Refer to State Rules

Report of Accident

Director/Inspector of Factories

7 days of incident

the Workmen's Compensation act, 1923

EE

Report of serious bodily injuries/fatal accidents

Concerned Commissioner of Workmen's compensation

  <h2 style="color: #000000; text-align: center;"><span style="color: #0000ff; font-size: xx-large; font-family: comic sans ms,sans-serif;">The Checklist For Employers</span></h2>
<table cellpadding="5" border="0" align="center" style="padding-top: 10px; width: 96%;" cellspacing="0">
<tbody>
<tr>
<td>
<table cellpadding="8" style="width: 100%;" cellspacing="1">
<tbody>
<tr>
<td align="center"><span style="font-size: large;"><strong style="color: #000000;">Statutory Deposits,Returns and Notices</strong></span></td>
</tr>
<tr>
<td>
<table cellpadding="5" style="width: 100%;" cellspacing="1">
<tbody>
<tr>
<td align="center"><span style="font-size: large;"><strong style="color: #000000;">Month wise Checklist for Submission of Various Returns</strong></span></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td align="center"><span style="font-size: medium;"><strong style="color: #000000;">Note: After being ONLINE system as the decision of the Head Quarter office of ESIC and Provident Fund not to be submits the entire returns for the act right now.</strong></span></td>
</tr>
<tr>
<td>
<table cellpadding="5" border="1" bgcolor="#ffffff" style="text-align: center; width: 100%;" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#ffffff" align="right" width="20%"><strong style="color: #000000; text-align: right;">MONTH &amp;<br /> DATE-WISE</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong style="color: #000000;">NAME OF THE STATUE</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong style="color: #000000;">FORM</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong style="color: #000000;">NAME OF RETURN/COMPLIANCE</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong style="color: #000000;">TO BE SENT TO</strong></td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">January</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Employment Exchanges (CNV) Act,1959 &amp; Rules</td><td bgcolor="#ffffff" align="center">ER-I<br /> (Rule 6)</td><td bgcolor="#ffffff" align="center">Quarterly Return for quarter <br /> ended</td><td bgcolor="#ffffff" align="center">Local Employment Exchange</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">Gujarat/Delhi/Bombay/ Labour Welfare fund act,1953</td><td bgcolor="#ffffff" align="center">Annexure "A"</td><td bgcolor="#ffffff" align="center">Statement of contribution of along with chq.</td><td bgcolor="#ffffff" align="center">Gujarat labour welfare fund Board/ Commissioner</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">21</td><td bgcolor="#ffffff" align="center">The Maternity Benefit Act, 1961</td><td bgcolor="#ffffff" align="center">LMNO<br /> Rule 16(1)</td><td bgcolor="#ffffff" align="center">Annual Returns and details <br /> of payment ending 31st Dec.</td><td bgcolor="#ffffff" align="center">Competent authority<br /> under the Act</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">30</td><td bgcolor="#ffffff" align="center">The Contract Labour 9 (R&amp;A) Act, 1970 &amp; Rules</td><td bgcolor="#ffffff" align="center">XXIV<br /> Rule 82(1)</td><td bgcolor="#ffffff" align="center">Half yearly return by contractor <br /> (Indupllcate)</td><td bgcolor="#ffffff" align="center">Concerned Licensing Officer</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">31</td><td bgcolor="#ffffff" align="center">The Factories Act, 1948</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">Chief Inspector/Director<br /> or other competent<br /> authority of the area</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">February</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">1</td><td bgcolor="#ffffff" align="center">The minimum Wages Act, 1948</td><td bgcolor="#ffffff" align="center">III Rule 21 (4A)</td><td bgcolor="#ffffff" align="center">Annual Return</td><td bgcolor="#ffffff" align="center">Inspector under the are<br /> concerned</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">1</td><td bgcolor="#ffffff" align="center">The Payment of Wages Act,1936 &amp; related Mines Rules</td><td bgcolor="#ffffff" align="center">V Rule 18 (related Mines Rules) VII, VIII Rule 16 related Air Transport</td><td bgcolor="#ffffff" align="center">Annual Return</td><td bgcolor="#ffffff" align="center">Concerned Labour<br /> Commissioner</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">Concerned Regional<br /> Commissioner</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">April</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Apprenticeship Act, 1961</td><td bgcolor="#ffffff" align="center">APP-2</td><td bgcolor="#ffffff" align="center">Half yearly return March ending</td><td bgcolor="#ffffff" align="center">Concerned Regional Director/Apprenticeship Advisor</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Employment Exchange (CNV) Act, 1959 &amp; Rules</td><td bgcolor="#ffffff" align="center">ER-I Rule 6</td><td bgcolor="#ffffff" align="center">Quarterly return for quarter ended 31 Dec.</td><td bgcolor="#ffffff" align="center">Local Employment Exchange</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">30</td><td bgcolor="#ffffff" align="center">The employees Provident fund act,1952</td><td bgcolor="#ffffff" align="center">3A &amp; 6A</td><td bgcolor="#ffffff" align="center">Annual individual Returns &amp; Returns of Contributions.</td><td bgcolor="#ffffff" align="center">Regional PF Commissioner, SURAT.</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">May</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">12</td><td bgcolor="#ffffff" align="center">The Employees State Insurance Act, 1948, Rules &amp; Regulations</td><td bgcolor="#ffffff" align="center">6 Sec.44 Regulation 26</td><td bgcolor="#ffffff" align="center">Summary of contribution in quadruplicate a/w challans Monthly return a/w cheque</td><td bgcolor="#ffffff" align="center">Respective local office or any Scheduled Bank Now it is online hence not submitted.</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">July</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Employment Exchanges (CNV) Act, 1959 &amp; Rules</td><td bgcolor="#ffffff" align="center">ER-I Rule 6</td><td bgcolor="#ffffff" align="center">Quarterly return for the quarter ended</td><td bgcolor="#ffffff" align="center">Concerned Employment Officer</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Factories Act, 1948</td><td bgcolor="#ffffff" align="center">Refer to State Rules</td><td bgcolor="#ffffff" align="center">Half yearly return</td><td bgcolor="#ffffff" align="center">Concerned Director/Inspector</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Contract Labour (R&amp;A) Act, 1970</td><td bgcolor="#ffffff" align="center">XXIV</td><td bgcolor="#ffffff" align="center">Half yearly return by contractor</td><td bgcolor="#ffffff" align="center">Concerned Inspector</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">Gujarat/Delhi/Bombay Labour Welfare fund act</td><td bgcolor="#ffffff" align="center">Annexere "A"</td><td bgcolor="#ffffff" align="center">Statement of contribution of Jun. along with chq.</td><td bgcolor="#ffffff" align="center">Gujarat labour welfare fund</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">October</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Apprenticeship Act, 1961</td><td bgcolor="#ffffff" align="center">APP-2</td><td bgcolor="#ffffff" align="center">Half yearly return Sept, ending</td><td bgcolor="#ffffff" align="center">Dy. Apprenticeship Adviser</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">15</td><td bgcolor="#ffffff" align="center">The Employment Exchanges (CNV) Act, 1959 &amp; Rules</td><td bgcolor="#ffffff" align="center">ER-I Rule 6</td><td bgcolor="#ffffff" align="center">Quarterly return for the quarter ended</td><td bgcolor="#ffffff" align="center">Concerned Employment Officer</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">30</td><td bgcolor="#ffffff" align="center">The Factories Act, 1948 &amp; Rules</td><td bgcolor="#ffffff" align="center">Refer to State Rules</td><td bgcolor="#ffffff" align="center">Application for renewal of licence</td><td bgcolor="#ffffff" align="center">Director/Inspector of Factories</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">31</td><td bgcolor="#ffffff" align="center">The Contract Labour (R&amp;A)</td><td bgcolor="#ffffff" align="center">VII Rule 29(2)</td><td bgcolor="#ffffff" align="center">Application for renewal of licence</td><td bgcolor="#ffffff" align="center">Concerned Inspector</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">November</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">12</td><td bgcolor="#ffffff" align="center">The Employees State Insurance Act, 1948</td><td bgcolor="#ffffff" align="center">6 Sec.44 Regulation 25</td><td bgcolor="#ffffff" align="center">Summary of contribution in quadruplicate a/w challans</td><td bgcolor="#ffffff" align="center">Concerned local office. Now it is online hence not submitted.</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center" colspan="5">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center"><strong style="color: #000000;">December</strong></td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td><td bgcolor="#ffffff" align="center">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" align="center">30</td><td bgcolor="#ffffff" align="center">The Payment of Bonus Act, 1965 &amp; Rules</td><td bgcolor="#ffffff" align="center">D Rule 5</td><td bgcolor="#ffffff" align="center">Annual Return - Bonus paid to the employees for the accounting year ending</td><td bgcolor="#ffffff" align="center">Concerned inspector/under the Act</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5" style="padding-top: 10px; width: 100%;" cellspacing="1">
<tbody>
<tr>
<td align="center"><strong>Checklist for submission of Various Returns- Monthly/on Occurrence</strong></td>
</tr>
<tr>
<td>
<table cellpadding="5" border="1" bgcolor="#999999" style="width: 100%;" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#ffffff" align="center" width="20%"><strong>TIME LIMIT</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong>ACT</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong>FORM ETC</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong>NAME OF RETURN/COMPLIANCE</strong></td><td bgcolor="#ffffff" align="center" width="20%"><strong>TO BE SENT TO</strong></td>
</tr>
<tr>
<td bgcolor="#ffffff">21st of every month</td><td bgcolor="#ffffff">The Employees State Insurance Act, 1948</td><td bgcolor="#ffffff">Challans</td><td bgcolor="#ffffff">Remittance of Contributions</td><td bgcolor="#ffffff">Concerned Regional Office</td>
</tr>
<tr>
<td bgcolor="#ffffff">Every month 15th</td><td bgcolor="#ffffff">The Employees Provident Funds &amp; MP Act, 1952</td><td bgcolor="#ffffff">Challans</td><td bgcolor="#ffffff">Remittance of Contributions</td><td bgcolor="#ffffff">Concerned Regional Office</td>
</tr>
<tr>
<td bgcolor="#ffffff">&nbsp;</td><td bgcolor="#ffffff">&nbsp;</td><td bgcolor="#ffffff">&nbsp;</td><td bgcolor="#ffffff">&nbsp;</td><td bgcolor="#ffffff">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff">Immediately fatal/death &amp; within 48 hrs. in ordinary cases and immediately in death or serious injury cases</td><td bgcolor="#ffffff">The Employees State Insurance Act,1948</td><td bgcolor="#ffffff">16</td><td bgcolor="#ffffff">Accident Report</td><td bgcolor="#ffffff">Concerned local office/dispensary of ESI</td>
</tr>
<tr>
<td valign="top" bgcolor="#ffffff">Every month</td><td valign="top" bgcolor="#ffffff">The Apprenticeship Act, 1961</td><td valign="top" bgcolor="#ffffff" colspan="3">
<table cellpadding="5" bgcolor="#999999" style="width: 100%;" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#ffffff" width="30%">Printed Format</td><td bgcolor="#ffffff" width="30%">Bill for reimbursement</td><td bgcolor="#ffffff" width="30%">Apprenticeship Advisor</td>
</tr>
<tr>
<td bgcolor="#ffffff">4 Rule 14(4)</td><td bgcolor="#ffffff">With seven days of joining of Apprentice</td><td bgcolor="#ffffff">Regional Director/state Apprenticeship Advisor</td>
</tr>
<tr>
<td bgcolor="#ffffff">1A Report containing record of basic training etc.Rule 14 (5&amp;6)</td><td bgcolor="#ffffff">Report for the period March-15th April ending Sept. 15th October</td><td bgcolor="#ffffff">Regional Director/State Apprentices</td>
</tr>
<tr>
<td bgcolor="#ffffff">3 Rule 14(7)</td><td bgcolor="#ffffff">To be sent in Nov. &amp; May Progress Report</td><td bgcolor="#ffffff">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff">6 Rule 8</td><td bgcolor="#ffffff">Record of work done and the studies taken by Graduates Technicians etc.</td><td bgcolor="#ffffff">Regional director/State apprenticeship Advisor</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td bgcolor="#ffffff">4 hours of occurrence</td><td bgcolor="#ffffff">The Factories Act, 1948</td><td bgcolor="#ffffff">Refer to State Rules</td><td bgcolor="#ffffff">Notice of accident/dangerous occurrence</td><td bgcolor="#ffffff">Inspector/Director of Factories</td>
</tr>
<tr>
<td bgcolor="#ffffff">12 hours of occurrence</td><td bgcolor="#ffffff">The Factories Act, 1948</td><td bgcolor="#ffffff">Refer to State Rules</td><td bgcolor="#ffffff">Report of Accident</td><td bgcolor="#ffffff">Director/Inspector of Factories</td>
</tr>
<tr>
<td bgcolor="#ffffff">7 days of incident</td><td bgcolor="#ffffff">the Workmen's Compensation act, 1923</td><td bgcolor="#ffffff">EE</td><td bgcolor="#ffffff">Report of serious bodily injuries/fatal accidents</td><td bgcolor="#ffffff">Concerned Commissioner of Workmen's compensation</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p align="justify">&nbsp;</p>
</td>
</tr>
</tbody>
</table>
